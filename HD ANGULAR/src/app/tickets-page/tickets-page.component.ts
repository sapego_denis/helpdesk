import { Component, OnInit,  ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ticket } from '../shared/components/interfaces/ticket';
import {Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { TicketService } from './ticket.service';
import { MainLayoutComponent } from '../shared/components/main-layout/main-layout.component';
import { FormGroup, FormControl } from '@angular/forms';
import { TicketAction } from '../shared/components/interfaces/ticketAction';


@Component({
  selector: 'app-tickets-page',
  templateUrl: './tickets-page.component.html',
  styleUrls: ['./tickets-page.component.scss'],
  providers: [TicketService]
})
export class TicketsPageComponent implements OnInit {

  tickets: Ticket []
  dataSource: Ticket [];
  someAction: TicketAction;
  someActions: TicketAction [];
  displayedColumns: string[] = ['ID', 'name', 'Desired date', 'Urgency', 'Status', 'Action'];
 toggle = false;
 role: string;
  form: FormGroup;
  availableToCreateTicket = false;


  constructor(
    
    private mainLayout: MainLayoutComponent,
    private http: HttpClient,
    private ticketService: TicketService) { 
    this.dataSource = this.tickets;
    
  }

  getAllTickets (){
   
    this.ticketService.getAllTickets().subscribe ( tickets => {
      this.dataSource = tickets;  
      const data = this.tickets;
      console.log('data', data)
      console.log('Response', tickets)
      this.tickets =tickets 
       CTI:Number
      for (let ticket of this.tickets){
       this.someActions = ticket?.availableActions.doAction
       
      console.log('Some actions', this.someActions)}
      })

  }

  availableToCreateNewTicket(){
    this.role = localStorage.getItem('role')
    if (this.role!="ENGINEER") {this.availableToCreateTicket = true}

  }

  getMyTickets () {
    this.ticketService.getAllTicketsRole()
    .subscribe ( tickets => {
      console.log('Response', tickets)
      this.tickets =tickets  
      this.dataSource = tickets;  
  })
}


ngOnInit() {
  this.role = localStorage.getItem('role')
  this.availableToCreateNewTicket()
  this.mainLayout.ngOnInit()
  this.getAllTickets();
  this.form = new FormGroup ({
    doAction: new FormControl (''),
    })

}

doAction() {
  
  var text = this.form.value.doAction.split('|',2)
  var currentTicketId = text[0]
  var action = text[1]
 const newStatus: TicketAction = {
  doAction: action
 }
  console.log ('Currentid', action)
  this.ticketService.putActions(currentTicketId,action).subscribe(()=>{
    this.ngOnInit()
  })
}
  
  dataSourse (sort: Sort)
  {
    this.dataSource = this.tickets.slice();
    const data = this.tickets.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource = data;
      return;
    }
    this.dataSource = data.sort ((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'ID': return compare (a.ticketId, b.ticketId, isAsc);
        case 'name': return compare(a.name, b.name, isAsc);
        case 'Desired date': return compare (a.desiredResolutionDate, b.desiredResolutionDate, isAsc);
        case 'Urgency': return compare (a.urgency, b.urgency, isAsc);
        case 'Status': return compare (a.state, b.state, isAsc);
        default: return 0;        
    }
    function compare(a: number | string, b: number | string, isAsc: boolean) {
      return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }
  
    }
    )
  }
  
}