import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Ticket} from '../shared/components/interfaces/ticket';
import {Comment} from '../shared/components/interfaces/comment';
import {Category} from '../shared/components/interfaces/category';
import { Observable } from 'rxjs';
import { Feedback } from '../shared/components/interfaces/feedback';

const httpOptionsPlain = {
    headers: new HttpHeaders({
      'Accept': 'text/plain',
      'Content-Type': 'text/plain'
    }),
    'responseType': 'text'
  };
    
@Injectable()
export class TicketService{
    tickets: Ticket [];
    history: History [];
    comments: Comment [];
    categories: Category [];
    id: number;
    
    
    constructor(private http: HttpClient){ }

    public getAllTickets (): Observable <Ticket []>{
      return this.http.get <Ticket []>('http://localhost:8080/v1/tickets')
  
    }  
       
  public getTicket (id: number): Observable <Ticket>{
   return this.http.get <Ticket>(`http://localhost:8080/v1/tickets/${id}`)         
    }

  public getAllTicketsRole (): Observable <Ticket []>{
    return this.http.get <Ticket []>('http://localhost:8080/v1/tickets/role')

  }  

  public getMyTickets ():  Observable <Ticket []>{ 
    return  this.http.get <Ticket []>('http://localhost:8080/v1/tickets/myTickets')

  }

  public createTicket (newTicket: Ticket):  Observable <any> { 
    return  this.http.post <any>('http://localhost:8080/v1/tickets', newTicket)

  }

  public editTicket (id: number, updatedTicket: Ticket):  Observable <any> { 
    return  this.http.put <any>(`http://localhost:8080/v1/tickets/${id}`, updatedTicket)

  }

  public getComments (id: number):  Observable <Comment []> { 
    return  this.http.get <Comment []>(`http://localhost:8080/v1/comments/${id}`)

  }

  public addComment (newComment: Comment):  Observable <Comment> { 
    return  this.http.post <Comment>('http://localhost:8080/v1/comments', newComment)

  }

  getHistory (id: number):  Observable <any []> { 
    return  this.http.get <any []>(`http://localhost:8080/v1/history/${id}`)

  }

  public getAllCategories (): Observable <Category []>{
    return this.http.get <Category []>('http://localhost:8080/v1/categories')

  } 

  public putActions (id: number, action: string): Observable <any>{
    return this.http.put <any>(`http://localhost:8080/v1/tickets/changeStatus/${id}`,action)

  } 

  public getAttachment(id:number): Observable <any>{
    return  this.http.get (`http://localhost:8080/file/${id}`)
  }

  public createFeedback (id: number, feedback: Feedback):  Observable <any> { 
    return  this.http.post <any>(`http://localhost:8080/v1/feedback/${id}`, feedback)

  }



  }
