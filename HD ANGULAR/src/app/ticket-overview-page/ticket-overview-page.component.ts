import { Component, OnInit } from '@angular/core';
import { TicketService } from '../tickets-page/ticket.service';
import { Ticket } from '../shared/components/interfaces/ticket';
import { ActivatedRoute, Params } from '@angular/router';
import { Comment } from '../shared/components/interfaces/comment';
import { History } from '../shared/components/interfaces/history';
import { FormGroup, FormControl } from '@angular/forms';
import { Attachments } from '../shared/components/interfaces/attacments';
import { ImageService } from '../shared/components/services/image.service';



@Component({
  selector: 'app-ticket-overview-page',
  templateUrl: './ticket-overview-page.component.html',
  styleUrls: ['./ticket-overview-page.component.scss'],
  providers: [TicketService]
})


export class TicketOverviewPageComponent implements OnInit {
  data: string;
 ticket: Ticket;
 currentTicketId: number;
 comments: Comment [];
 history: History [];
 dataSourceComment: Comment [];
 dataSourceHistory: History [];
 displayedColumnsComment: string[] = ['Date', 'User', 'Comment'];
 displayedColumnsHistory: string[] = ['Date', 'User', 'Action', 'Description'];
 historyFirst: boolean;
 form: FormGroup;
 form2: FormGroup;
 currentUserId: number;
 id="";
  attachments: any [];
  attachment: Attachments;
  attach: string;
  feedbackIsAvailable: boolean;

 

  constructor(
    private imageService: ImageService,
    private router: ActivatedRoute,
    private ticketService: TicketService){}

  
  ngOnInit() {this.form = new FormGroup({
    newComment: new FormControl ('') 
    
  });

  this.form2 = new FormGroup({
    getImage: new FormControl ('') 
    
  });
    

    
    this.router.params.subscribe ((params: Params) => {
      console.log ('PArams', params)
      this.currentTicketId = params.id  
    
    this.ticketService.getTicket(params.id).subscribe( ticket => {
      console.log('Response', ticket)
      this.ticket =ticket;
      this.feedbackIsAvailable = ticket.feedbackIsAvailable;
    
      for (let attachment of ticket?.attachments){
        this.attachments = ticket?.attachments
            
        console.log('attachment', attachment)
      
       }
    
    });

      this.getHistory();
    this.historyFirst=true;
}) ; 

}
  someActions(arg0: string, someActions: any) {
    throw new Error("Method not implemented.");
  }

getComments(){
  this.ticketService.getComments (this.currentTicketId).subscribe (comments => {
    console.log ('Comments', comments)
    this.comments = comments
    this.dataSourceComment = this.comments;})
}

  

addComment(){
  const restCommet: Comment = {
    text: this.form.value.newComment,
    ticketId: this.currentTicketId,
    userId: this.currentUserId
  } 
  
  this.ticketService.addComment(restCommet).subscribe(()=>{
    this.form.reset()
    this.getComments()

  })
  
}
getHistory(){
  this.ticketService.getHistory (this.currentTicketId).subscribe (histories => {
    console.log ('History', histories)
    this.history = histories
   
    this.dataSourceHistory = this.history;})
}



  getImage() {
    const attachmentId: number = +this.form2.value.getImage
        console.log('attachment', attachmentId)
    this.imageService.getData(`http://localhost:8080/file/${attachmentId}`)
      .subscribe(
        imgData => this.data = imgData,
        err => console.log(err)
      );
  }

}
