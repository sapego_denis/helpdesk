import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './shared/components/main-layout/main-layout.component';
import { TicketsPageComponent } from './tickets-page/tickets-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { TicketOverviewPageComponent } from './ticket-overview-page/ticket-overview-page.component';
import { CreateTicketPageComponent } from './create-ticket-page/create-ticket-page.component';
import { FeedbackPageComponent } from './feedback-page/feedback-page.component';
import { EditTicketPageComponent } from './edit-ticket-page/edit-ticket-page.component';




const routes: Routes = [
  {
    path:'', component:MainLayoutComponent, children: [
      {path:'', redirectTo:'/', pathMatch: 'full'},
      {path: '', component: LoginPageComponent},
      {path:'tickets', component: TicketsPageComponent},
      {path: 'ticketoverview/:id', component: TicketOverviewPageComponent},
      {path: 'createTicket', component: CreateTicketPageComponent},
      {path: 'feedbacks/:id', component: FeedbackPageComponent},
      {path: 'editTicket/:id', component: EditTicketPageComponent}

      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
