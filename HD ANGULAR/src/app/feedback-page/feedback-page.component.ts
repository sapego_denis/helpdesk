import { Component, OnInit } from '@angular/core';
import { StarRatingComponent } from 'ng-starrating';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Feedback } from '../shared/components/interfaces/feedback';
import { TicketService } from '../tickets-page/ticket.service';
import { Ticket } from '../shared/components/interfaces/ticket';


@Component({
  selector: 'app-feedback-page',
  templateUrl: './feedback-page.component.html',
  styleUrls: ['./feedback-page.component.scss'],
  providers: [TicketService]
})
export class FeedbackPageComponent implements OnInit {
  currentTicketId: number;
  rating3: number;
  ticket: Ticket ;
  public form: FormGroup;
  constructor(
    private ticketService: TicketService,
    private router2: Router,
    private router: ActivatedRoute,
    private fb: FormBuilder) {
      this.rating3 = 0;
      this.form = this.fb.group({
        rating1:['', Validators.required],
        rating2:[4]
      });
     }

  ngOnInit() {
    this.form = new FormGroup({
      feedback: new FormControl (''),
   
      });
    this.router.params.subscribe ((params: Params) => {
      console.log ('PArams', params)
      this.currentTicketId = params.id 
      this.ticketService.getTicket(params.id).subscribe( tickets => {
        console.log('Response', tickets)
        this.ticket =tickets;});
      });
  }

  addFeedback(){
    const feedback: Feedback = {
      rate: this.rating3,
      text: this.form.value.feedback

    }
    console.log ('feedback', feedback)
    this.ticketService.createFeedback(this.currentTicketId, feedback).subscribe(()=>{
      console.log ("Feedback was added")
    })
    this.back();
  }

  back(){
    this.router2.navigate(['/ticketoverview', this.currentTicketId])
  }

 

}
