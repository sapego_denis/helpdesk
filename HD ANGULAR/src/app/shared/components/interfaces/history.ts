export class History{
    constructor (
      public id: number,
    public ticketId?: number,  
    public userId?: number,
    public description?: string,
    public dateOfStory?: number,
    
    public action?: string,
    public userFullName?: string,
    
    ) {}
  }
  