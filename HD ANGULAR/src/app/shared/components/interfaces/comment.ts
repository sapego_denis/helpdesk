export class Comment {
    constructor (
    
    public userId: number,
    public text?: string,
    public dateOfComment?: number,
    public ticketId?: number,
    public userFullName?: string,
    
    ) {}
  }