export class Ticket {
    id?: number;
  availableActions?: any;
    constructor (
    
    
    public name: string,
    public categoryNum: number,
    public ticketId?: number,
    public description?: string,
    public desiredResolutionDate?: string,
    public urgency?: string,
    public state?: string,
    public text?: string,
    public attachments?: string [],
    public feedbackIsAvailable?: boolean,
    
    ) {}
  }

  


 