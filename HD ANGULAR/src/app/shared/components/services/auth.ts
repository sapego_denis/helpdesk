import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from '../interfaces/user';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({providedIn: 'root'})

export class AuthService {
url = 'http://localhost:8080/login'
jwtToken;



    constructor (private http: HttpClient) {}

    get token (): string {
        const expDate = new Date (localStorage.getItem('expires'))
        if (new Date() >expDate) {
            this.logout()
            return null
        }
        return localStorage.getItem ('Authorization')
    }

    login (user: User): Observable <any> {
        user.returnSecureToken = true
        return this.http.post <any>(`http://localhost:8080/login`, user)
        .pipe(
            tap (this.setToken)
        )
   }

    logout (){
        this.setToken(null)
    }

     isAuthenticated(): boolean {
        return !!this.token
    }

   private  setToken(resp) {
       if (resp) {
        const expDate = new Date (new Date().getTime()+ +resp.expiresIn*1000)
        localStorage.setItem ('Authorization', resp.token)
        localStorage.setItem ('username', resp.username)
        localStorage.setItem ('role', resp.role)
        localStorage.setItem ('expires', expDate.toString())
       } else {
           localStorage.clear()
       }

    }
}

