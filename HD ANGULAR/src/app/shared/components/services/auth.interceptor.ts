import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { AuthService } from './auth';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor (
        private auth: AuthService,
        private rout: Router
    ){}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable <HttpEvent<any>> {
        if (this.auth.isAuthenticated()) {
            req = req.clone ({
                headers: req.headers.append ('Authorization', 'Bearer_'+this.auth.token)
            })
        }
        return next.handle (req)
        .pipe (
            tap ( () =>{
                console.log ('Intercept')
            }

            )
        )
        throw new Error("Method not implemented.");
    }
}
    
