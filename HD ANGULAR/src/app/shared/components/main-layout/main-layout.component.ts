import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {
username:string
role: string
  constructor(
    private router: Router,
    private auth: AuthService
  ) { }

  ngOnInit() {
    
    this.username = localStorage.getItem('username')
    this.role = localStorage.getItem('role')
    
  }

  logout (event: Event){
    event.preventDefault();
    this.auth.logout()
    this.role =''
    this.username=''
    this.router.navigate (['/'])
    this.ngOnInit()
  }

}
