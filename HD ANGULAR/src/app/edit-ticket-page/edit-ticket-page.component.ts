import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TicketService } from '../tickets-page/ticket.service';
import { Ticket } from '../shared/components/interfaces/ticket';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {Category} from '../shared/components/interfaces/category';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-edit-ticket-page',
  templateUrl: './edit-ticket-page.component.html',
  styleUrls: ['./edit-ticket-page.component.scss'],
  providers: [TicketService]
})
export class EditTicketPageComponent implements OnInit {
ticketId: number;
ticket: Ticket ;
selectedFile: File = null;
startDate = new Date(Date.now());
categories: Category [];
form: FormGroup;
constructor(
  private router: ActivatedRoute,
  private router2: Router,
  private ticketService: TicketService,
  private http: HttpClient,
) { }

ngOnInit() {
  this.getCategories();
  this.router.params.subscribe ((params: Params) => {
    console.log ('PArams', params)
    this.ticketId = params.id
    this.ticketService.getTicket(params.id).subscribe( tickets => {
      console.log('Response', tickets)
      this.ticket =tickets;});});

  this.form = new FormGroup({
    category: new FormControl ('',[Validators.required]),
    name: new FormControl ('',[Validators.required]),
    urgency: new FormControl ('',[Validators.required]),
    description: new FormControl (null),
    resolutionDate: new FormControl (null, [Validators.required]),
    state: new FormControl (null),
    commentText: new FormControl (''),
    
  });
}

editTicket() {
  
  console.log('Form Data: ', this.form)
  const ticketData: Ticket = {
    categoryNum: +this.form.value.category,
    name: this.form.value.name,
    urgency: this.form.value.urgency,
    description: this.form.value.description,
    desiredResolutionDate: this.form.value.resolutionDate,
    state:  this.form.value.state ='NEW',
    text: this.form.value.commentText,
    
    
 }
 console.log('Form Data: ', ticketData)
 this.ticketService.editTicket(this.ticketId, ticketData).subscribe(()=>{
  this.onUpload(this.ticketId)
  this.form.reset()
 
})

console.log('Form Data: ', ticketData)

}

saveTicketAsDraft() {
  this.form.value.state ='DRAFT'
  this.editTicket()
  }

  getCategories() {
    this.ticketService.getAllCategories().subscribe ( categories => {
      console.log('Response', categories)
      this.categories =categories;   })
  }

  onUpload(ticketID:number){
    const fd = new FormData();
    fd.append('file', this.selectedFile, this.selectedFile.name);
    this.http.post(`http://localhost:8080/file/upload/${ticketID}`, fd)
    .subscribe(res => {
      console.log('Image was uploaded', res)
    });
  }

  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
  }
}
