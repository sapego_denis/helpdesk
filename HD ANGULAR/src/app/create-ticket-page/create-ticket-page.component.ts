import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Ticket } from '../shared/components/interfaces/ticket';
import {Category} from '../shared/components/interfaces/category';
import { TicketService } from '../tickets-page/ticket.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-create-ticket-page',
  templateUrl: './create-ticket-page.component.html',
  styleUrls: ['./create-ticket-page.component.scss'],
  providers: [TicketService]
})
export class CreateTicketPageComponent implements OnInit {

  selectedFile: File = null;

  startDate = new Date(Date.now());
  categories: Category [];

  form: FormGroup;
  formData: FormData;
  constructor(
    private router: Router,
    private ticketService: TicketService,
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.getCategories();
    this.form = new FormGroup({
      category: new FormControl ('',[Validators.required]),
      name: new FormControl ('',[Validators.required, Validators.maxLength(100)]),
      urgency: new FormControl ('',[Validators.required]),
      description: new FormControl (null),
      resolutionDate: new FormControl (null, [Validators.required]),
      state: new FormControl (''),
      commentText: new FormControl (''),
     
    });
  }

  createTicket() {
    var newTicketID:number;
    console.log('Form Data: ', this.form)
    const ticketData: Ticket = {
      categoryNum: +this.form.value.category,
      name: this.form.value.name,
      urgency: this.form.value.urgency,
      description: this.form.value.description,
      desiredResolutionDate: this.form.value.resolutionDate,
      state:  this.form.value.state ='NEW',
      text: this.form.value.commentText,
      
      
   }
   console.log('Form Data: ', ticketData)
   this.ticketService.createTicket(ticketData).subscribe((resp: number)=>{
     newTicketID = resp
     console.log ('New ticket Id', newTicketID)
    this.router.navigate(['/tickets'])
    this.onUpload(newTicketID)
    this.form.reset()
    
    
   })

    console.log('Form Data: ', ticketData)
  }

  saveTicketAsDraft() {
     console.log('Form Data: ', this.form)
  const ticketDataDraft: Ticket = {
    categoryNum: +this.form.value.category,
    name: this.form.value.name,
    urgency: this.form.value.urgency,
    description: this.form.value.description,
    desiredResolutionDate: this.form.value.resolutionDate,
    state:  this.form.value.state ='DRAFT',
    text: this.form.value.commentText
    
 }
 console.log('Form Data: ', ticketDataDraft)
 this.ticketService.createTicket(ticketDataDraft).subscribe(()=>{
  this.form.reset()
 })

  console.log('Form Data: ', ticketDataDraft)
    }

  getCategories() {
    this.ticketService.getAllCategories().subscribe ( categories => {
      console.log('Response', categories)
      this.categories =categories;   })
  }

  onUpload(newTicketID:number){
    const fd = new FormData();
    fd.append('file', this.selectedFile, this.selectedFile.name);
    this.http.post(`http://localhost:8080/file/upload/${newTicketID}`, fd)
    .subscribe(res => {
      console.log('Image was uploaded', res)
    });
  }

  onFileSelected(event){
    this.selectedFile = <File> event.target.files[0];
  }
}
