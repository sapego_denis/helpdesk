import { NgModule } from '@angular/core';
import {MatButtonModule,
  MatTableModule,
  MatButtonToggleModule,
  MatSelectModule,
  MatSortModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogModule
  } from '@angular/material';


const MaterialComponents = [
  MatButtonModule,
  MatTableModule,
  MatSelectModule,
  MatSortModule,
  MatButtonToggleModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule,
  MatDialogModule
]

@NgModule({
   imports: [MaterialComponents],
   exports: [MaterialComponents]
})
export class MaterialModule { }
