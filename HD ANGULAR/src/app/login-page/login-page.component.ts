import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../shared/components/interfaces/user';
import { AuthService } from '../shared/components/services/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  form: FormGroup
  

  constructor(
    private auth: AuthService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.form = new FormGroup ({
      login: new FormControl (null, [Validators.email, Validators.required]),
      password:  new FormControl (null, [Validators.required, Validators.minLength(6)])
    })
  }
submit() {
  if (this.form.invalid) {
    return console.log("Form invalid")
  }
  const user: User = {
    username: this.form.value.login,
    password: this.form.value.password
  }
  this.auth.login(user).subscribe (
    ()=> {
      this.router.navigate (['/tickets']);
    })
  }
}
