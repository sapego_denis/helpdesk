import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Provider } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainLayoutComponent } from './shared/components/main-layout/main-layout.component';
import { TicketsPageComponent } from './tickets-page/tickets-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { MaterialModule } from './material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { TicketOverviewPageComponent } from './ticket-overview-page/ticket-overview-page.component';
import { CreateTicketPageComponent } from './create-ticket-page/create-ticket-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FeedbackPageComponent } from './feedback-page/feedback-page.component';
import { NgxStarRatingModule } from 'ngx-star-rating';
import { EditTicketPageComponent } from './edit-ticket-page/edit-ticket-page.component';
import { AuthService } from './shared/components/services/auth';
import { SharedModule } from './shared/components/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './shared/components/services/auth.interceptor';
import { DialogComponent } from './dialog/dialog.component';

const INTERSEPTOR_PROVIDER: Provider ={
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: AuthInterceptor
}



@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    TicketsPageComponent,
    LoginPageComponent,
    TicketOverviewPageComponent,
    CreateTicketPageComponent,
    FeedbackPageComponent,
    EditTicketPageComponent,
    DialogComponent,
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxStarRatingModule,
    SharedModule,
    
    
  ],
  providers: [AuthService, INTERSEPTOR_PROVIDER],
  bootstrap: [AppComponent]
})
export class AppModule { }
