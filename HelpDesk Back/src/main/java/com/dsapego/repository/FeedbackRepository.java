package com.dsapego.repository;

import com.dsapego.model.Feedback;

import java.util.List;

public interface FeedbackRepository {
    List<Feedback> findAll();
    void update(Feedback feedback);
    void delete(Feedback feedback);
    void save(Feedback feedback);
}

