package com.dsapego.repository;

import com.dsapego.model.user.Role;
import com.dsapego.model.user.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    Optional<User> getUserByEmail (String login);
    Optional<User> getByKey (Long id);
    List<User> findAll();
    List<User> getAllUsersByRole(Role role);
}
