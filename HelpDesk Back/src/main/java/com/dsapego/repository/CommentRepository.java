package com.dsapego.repository;

import com.dsapego.model.Comment;


import java.util.List;

public interface CommentRepository {
    List<Comment> findAll();
    List <Comment> getCommentsForTicket(Long ticketID);
    void update(Comment comment);
    void delete(Comment comment);
    void save(Comment comment);
}
