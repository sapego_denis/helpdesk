package com.dsapego.repository;

import com.dsapego.model.History;

import java.util.List;

public interface HistoryRepository {
    List<History> findAll();
    List<History> getHistoryForTicket(Long ticketID);
    void update(History history);
    void delete(History history);
    void save(History history);

}
