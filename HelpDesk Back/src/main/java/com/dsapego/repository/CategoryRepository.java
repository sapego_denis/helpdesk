package com.dsapego.repository;

import com.dsapego.model.Category;

import java.util.List;
import java.util.Optional;


public interface CategoryRepository {
    List<Category> findAll();
    Optional<Category> getByKey (Long id);
    void update(Category category);
    void delete(Category category);
    void save(Category category);
}
