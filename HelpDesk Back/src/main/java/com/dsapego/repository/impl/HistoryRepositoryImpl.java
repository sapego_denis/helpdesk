package com.dsapego.repository.impl;

import com.dsapego.model.History;
import com.dsapego.repository.HistoryRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class HistoryRepositoryImpl extends AbstractRepository <Long, History> implements HistoryRepository {

    @Override
    public List<History> getHistoryForTicket(Long ticketID) {
        String jpQueryGetHistory = "from History c " +
                "where c.ticket.id = :id ";
        return entityManager.createQuery(jpQueryGetHistory, History.class)
                .setParameter("id", ticketID)
                .getResultStream()
                .collect(Collectors.toList());
    }
}
