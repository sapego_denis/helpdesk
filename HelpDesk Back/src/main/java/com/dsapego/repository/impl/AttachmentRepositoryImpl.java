package com.dsapego.repository.impl;

import com.dsapego.model.Attachment;
import com.dsapego.repository.AttachmentRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AttachmentRepositoryImpl extends AbstractRepository <Long, Attachment> implements AttachmentRepository {
    private static final String HQL_GET_ATTACHMENT_BY_TICKET_ID = "from Attachment a where a.ticket.id = :id";


    @Override
    public List<Attachment> getAttachmentsByTicketID(Long ticketID) {
        return entityManager.createQuery(HQL_GET_ATTACHMENT_BY_TICKET_ID, Attachment.class)
                .setParameter("id", ticketID)
                .getResultStream()
                .collect(Collectors.toList());
    }
}
