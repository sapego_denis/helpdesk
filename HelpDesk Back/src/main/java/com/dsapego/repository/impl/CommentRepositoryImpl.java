package com.dsapego.repository.impl;

import com.dsapego.model.Comment;
import com.dsapego.repository.CommentRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CommentRepositoryImpl extends AbstractRepository <Long, Comment> implements CommentRepository {

    @Override
    public List<Comment> getCommentsForTicket(Long ticketID) {
        String jpQueryGetComments = "from Comment c " +
                "where c.ticket.id = :id ";
        return entityManager.createQuery(jpQueryGetComments, Comment.class)
                .setParameter("id", ticketID)
                .getResultStream()
                .collect(Collectors.toList());
    }

}
