package com.dsapego.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository <P extends Serializable, T> {

    private Class<T> clazz;

    @PersistenceContext
    EntityManager entityManager;

     AbstractRepository(){
        this.clazz =(Class <T>)((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    public List <T> findAll(){
        return entityManager.createQuery("from "+clazz.getName() )
                .getResultList();
    }

    protected EntityManager getEntityManager() {
        return this.entityManager;
    }
    public Optional<T> getByKey(P key) {
        return Optional.ofNullable(entityManager.find(clazz, key));
    }
    public void save (T entity) {entityManager.persist(entity);}
    public void update (T entity) {entityManager.merge(entity);}
    public void delete(T entity) {entityManager.remove(entity);}
}
