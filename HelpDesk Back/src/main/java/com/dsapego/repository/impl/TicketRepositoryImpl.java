package com.dsapego.repository.impl;

import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;
import com.dsapego.repository.TicketRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class  TicketRepositoryImpl extends AbstractRepository <Long, Ticket> implements TicketRepository {

private static final String TICKET_BY_OWNERID = "from Ticket t where t.ownerID.id = :id ";

    @Override
    public List<Ticket> findAll() {
        return entityManager.createQuery("from Ticket", Ticket.class).getResultList();
    }

    @Override
    public List<Ticket> findAllTicketsForManager(User user) {
        String jpQueryManager = TICKET_BY_OWNERID +
                "or (t.ownerID.role='EMPLOYEE' and t.stateId='NEW') " +
                "or (t.approverID.id= t.ownerID.id and t.stateId in ('APPROVED','DECLINED','CANCELLED','IN_PROGRESS','DONE'))";
        return entityManager.createQuery(jpQueryManager,Ticket.class)
                .setParameter("id",user.getId())
                .getResultList();
    }
    @Override
    public List <Ticket> findAllTicketsForEngineer(User user){
        String jpQueryEngineer = TICKET_BY_OWNERID+
                "or (t.stateId = 'APPROVED' and t.stateId.role in ('MANAGER', 'EMPLOYEE')) " +
                "or (t.assigneeID = :id and t.stateId in ('IN_PROGRESS', 'DONE')) ";
        return entityManager.createQuery(jpQueryEngineer, Ticket.class)
                .setParameter("id", user.getId())
                .getResultList();
    }

    @Override
    public List <Ticket> findAllTicketsForEmployee(User user){

        return entityManager.createQuery(TICKET_BY_OWNERID, Ticket.class)
                .setParameter("id", user.getId())
                .getResultList();
    }




    @Override
    public List<Ticket> findAllByOwnerId(User user) {
        return entityManager.createQuery(TICKET_BY_OWNERID,Ticket.class)
                .setParameter("id", user.getId())
                .getResultList();
    }

}


