package com.dsapego.repository.impl;

import com.dsapego.model.Feedback;
import com.dsapego.repository.FeedbackRepository;
import org.springframework.stereotype.Repository;

@Repository
public class FeedbackRepositoryImpl extends AbstractRepository <Long, Feedback> implements FeedbackRepository {
}
