package com.dsapego.repository.impl;

import com.dsapego.model.user.Role;
import com.dsapego.model.user.User;
import com.dsapego.repository.UserRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends AbstractRepository <Long, User> implements UserRepository  {

    private static final String HQL_GET_USER_BY_EMAIL = "from User u where u.email = :email";
    private static final String HQL_GET_RECIPIENTS_BY_ROLE = "from User u where u.role = :role";


    public UserRepositoryImpl() {super(); }

    @Override
    public Optional <User> getUserByEmail(String email) {

        User user;
        user = entityManager.createQuery(HQL_GET_USER_BY_EMAIL, User.class)
                .setParameter("email", email)
                .getResultStream()
                .findFirst()
                .orElse(null);

        return Optional.ofNullable(user);
    }
    @Override
public List<User> getAllUsersByRole(Role role){
        return entityManager.createQuery(HQL_GET_RECIPIENTS_BY_ROLE, User.class)
                .setParameter("role", role)
                .getResultList();
        }




}
