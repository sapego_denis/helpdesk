package com.dsapego.repository.impl;

import com.dsapego.model.Category;
import com.dsapego.repository.CategoryRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepositoryImpl extends AbstractRepository <Long, Category> implements CategoryRepository {
}
