package com.dsapego.repository;

import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;

import java.util.List;
import java.util.Optional;

public interface TicketRepository {

    List<Ticket> findAll();

    void update(Ticket ticket);
    void delete(Ticket ticket);
    void save(Ticket ticket);

    List<Ticket> findAllTicketsForManager(User user);

    List <Ticket> findAllTicketsForEngineer(User user);

    List <Ticket> findAllTicketsForEmployee(User user);

    Optional<Ticket> getByKey (Long id);

    List<Ticket> findAllByOwnerId(User user);






}
