package com.dsapego.repository;

import com.dsapego.model.Attachment;

import java.util.List;
import java.util.Optional;

public interface AttachmentRepository {
    List<Attachment> findAll();
    void update(Attachment attachment);
    void delete(Attachment attachment);
    void save(Attachment attachment);
    Optional<Attachment> getByKey(Long id);
    List<Attachment> getAttachmentsByTicketID(Long ticketID);
}
