package com.dsapego.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
@PropertySource(value = "classpath:mail.properties")
public class MailConfig {

    @Value("${localhost}")
    private String localhost;

    @Value("${smtp}")
    private String smtp;

    @Value("${USERNAME}")
    private String username;

    @Value("${PASSWORD}")
    private String password;

    @Value("${port}")
    private Integer port;

    @Value("${mail.smtp.starttls.enable}")
    private String mailSmtpStarttlsEnable;

    @Value("${mail.smtp.auth}")
    private String mailSmtpAuth;

    @Value("${mail.smtp.quitwai}")
    private String mailSmtpQuitwai;



    @Bean
    public JavaMailSenderImpl javaMailSender() {
        final JavaMailSenderImpl sender = new JavaMailSenderImpl();

        sender.setHost(localhost);
        sender.setPort(port);
        sender.setProtocol(smtp);
        sender.setUsername(username);
        sender.setPassword(password);

        final Properties sendProperties = new Properties();
        sendProperties.setProperty("mail.smtp.auth", mailSmtpAuth);
        sendProperties.setProperty("mail.smtp.starttls.enable", mailSmtpStarttlsEnable);
        sendProperties.setProperty("mail.smtp.quitwai", mailSmtpQuitwai);
        sender.setJavaMailProperties(sendProperties);

        return sender;
    }
}
