package com.dsapego.web;


import com.dsapego.dto.TicketDTO;
import com.dsapego.exceptions.MailTerminalExeption;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.services.TicketDTOService;
import com.dsapego.services.TicketService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/v1/tickets")
public class TicketResource {


    private final TicketDTOService ticketDTOService;
    private final TicketService ticketService;

    public TicketResource(TicketDTOService ticketDTOService, TicketService ticketService) {
        this.ticketDTOService = ticketDTOService;
        this.ticketService = ticketService;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
        public ResponseEntity<TicketDTO> findTicket (@PathVariable Long id) throws NotFoundException {
        return ResponseEntity.ok(ticketDTOService.findTicketDTOByID(id));
    }

    @GetMapping(value = "/role", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TicketDTO>> findAllTicketsForRole() {
        return ResponseEntity.ok(ticketDTOService.findAllTicketsDTOAccordingToRole());
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<List<TicketDTO>> findAllTickets() {
       return ResponseEntity.ok(ticketDTOService.findTicketsDTO());
    }

    @GetMapping(value = "/myTickets",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TicketDTO>> findMyTickets() {

        return ResponseEntity.ok(ticketDTOService.findTicketsDTOofCurrentUser());
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Long> deleteTicket(@PathVariable Long id) throws NotFoundException {
        Ticket ticketByID = ticketService.findTicketByID(id);
        boolean isRemoved = ticketService.deleteTicket(ticketByID);
        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(id, HttpStatus.OK);
    }


@PostMapping ( produces = MediaType.APPLICATION_JSON_VALUE)
public ResponseEntity createTicket( @RequestBody TicketDTO dto) throws NotFoundException {
    Long ticketId =ticketService.createNewTicket(dto);
    return ResponseEntity.ok(ticketId);

}
    @PutMapping(value = "/{ticketId}", produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity editTicket(@PathVariable Long ticketId, @RequestBody TicketDTO dto) throws MailTerminalExeption {


        dto.setId(ticketId);
       ticketDTOService.editTicket(dto);
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }

    @PutMapping(value = "/changeStatus/{id}", produces = MediaType.APPLICATION_JSON_VALUE )
    public ResponseEntity changeStatus(@PathVariable Long id, @RequestBody  String message) throws NotFoundException {
       ticketService.changeTicketStatus(id, message);
       return ResponseEntity.ok(HttpStatus.ACCEPTED);
    }


}
