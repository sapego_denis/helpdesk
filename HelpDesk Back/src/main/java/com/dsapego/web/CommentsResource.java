package com.dsapego.web;


import com.dsapego.dto.CommentDTO;
import com.dsapego.model.Comment;
import com.dsapego.services.CommentDTOService;
import com.dsapego.services.CommentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/v1/comments")
public class CommentsResource {

    private final CommentDTOService commentDTOService;
    private final CommentService commentService;

    public CommentsResource(CommentDTOService commentDTOService, CommentService commentService) {
        this.commentDTOService = commentDTOService;
        this.commentService = commentService;
    }


    @GetMapping( produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Comment>> findAll () {
        return ResponseEntity.ok(commentService.findAll());
    }

    @PostMapping ( produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createTicket( @RequestBody CommentDTO dto){
        commentService.createNewComment(dto);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
     public ResponseEntity <List<CommentDTO>> findCommentsForTicket (@PathVariable Long id)
    {
        return ResponseEntity.ok(commentDTOService.findCommentsForTicket(id));
    }

}
