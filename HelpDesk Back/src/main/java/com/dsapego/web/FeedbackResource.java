package com.dsapego.web;

import com.dsapego.dto.FeedbackDTO;
import com.dsapego.exceptions.MailTerminalExeption;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.services.FeedbackServiceDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/v1/feedback")
public class FeedbackResource {


    private final FeedbackServiceDTO feedbackServiceDTO;

    public FeedbackResource(FeedbackServiceDTO feedbackServiceDTO) {
        this.feedbackServiceDTO = feedbackServiceDTO;
    }

    @PostMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createFeedback(@PathVariable Long id, @RequestBody FeedbackDTO dto) throws NotFoundException, MailTerminalExeption {
        feedbackServiceDTO.createFeedback(id ,dto);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }

}
