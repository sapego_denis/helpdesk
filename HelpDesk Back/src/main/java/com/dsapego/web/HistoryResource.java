package com.dsapego.web;




import com.dsapego.dto.HistoryDTO;
import com.dsapego.services.HistoryDTOService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/v1/history")
public class HistoryResource {

    private final HistoryDTOService historyDTOService;


    public HistoryResource(HistoryDTOService historyDTOService) {
        this.historyDTOService = historyDTOService;

    }


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity <List<HistoryDTO>> findHistoryForTicket (@PathVariable Long id)
    {
        return ResponseEntity.ok(historyDTOService.findHistoryForTicket(id));
    }

}
