package com.dsapego.web;


import com.dsapego.exceptions.NotFoundException;
import com.dsapego.exceptions.UploadFileException;
import com.dsapego.model.Attachment;
import com.dsapego.services.AttachmentService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@Controller
public class AttachmentResource {


    private final AttachmentService attachmentService;

    public AttachmentResource(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }


    @PostMapping (value = "/file/upload/{id}" )
    public ResponseEntity uploadMultipartFile (@PathVariable Long id, MultipartFile file) throws UploadFileException {
        attachmentService.create(file, id);
        return ResponseEntity.ok(HttpStatus.ACCEPTED);
         }

    @GetMapping (value = "file/{id}")
    public ResponseEntity <byte[]> getFile(@PathVariable Long id) throws NotFoundException {
        Attachment attachment = attachmentService.findByID(id);
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(attachment.getMimetype()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + attachment.getName() + "\"")
                .body(attachment.getBlob());
    }


}
