package com.dsapego.web;

import com.dsapego.dto.CategoryDTO;
import com.dsapego.services.CategoryDTOService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/v1/categories")
public class CategoriesResource {


    private final CategoryDTOService categoryDTOService;

    public CategoriesResource(CategoryDTOService categoryDTOService) {
        this.categoryDTOService = categoryDTOService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CategoryDTO>> getAllCategories() {
        return ResponseEntity.ok(categoryDTOService.findAllCategoriesDTO());
    }
}
