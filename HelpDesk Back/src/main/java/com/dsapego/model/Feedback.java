package com.dsapego.model;

import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table (name ="feedbacks")
public class Feedback {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Long id;

    @Column (name = "rate")
    private Integer rate;

    @Column (name = "date")
    private LocalDateTime date;

    @Column (name = "text")
    private String text;

    @OneToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "user_id", nullable = false)
    private User user;

    @OneToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "ticket_id", nullable = false)
    private Ticket ticket;



    //GETTERS and SETTERS

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public  LocalDateTime getDate() {
        return date;
    }

    public void setDate( LocalDateTime date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
