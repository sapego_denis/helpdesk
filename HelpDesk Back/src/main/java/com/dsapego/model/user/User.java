package com.dsapego.model.user;

import com.dsapego.model.Comment;
import com.dsapego.model.History;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table (name ="users")
public class User  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private  Long id;

    @Column (name ="first_name")
    private String firstName;

    @Column (name ="last_name")
    private String lastName;

    @Column (name = "role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column (name ="email")
    private String email;

    @Column(name ="password")
    private  String password;

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "user", orphanRemoval = true)
    private List<Comment> comments = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "user", orphanRemoval = true)
    private List<History> history = new ArrayList<>();



    public User() {
    }

    //GETTERS and SETTERS


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List <Comment> getComments() {
        return comments;
    }

    public void setComments(List <Comment> comments) {
        this.comments = comments;
    }

    public List<History> getHistory() { return history;  }

    public void setHistory(List<History> history) {this.history = history;}
}
