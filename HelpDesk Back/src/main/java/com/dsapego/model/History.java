package com.dsapego.model;

import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table (name ="histories")
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private  Long id;

    @Column (name = "dateOfStory")
    private LocalDateTime date;

    @Column (name = "ticketAction")
    private String action;

    @Column (name = "description")
    private String description;

    @ManyToOne
    private User user;

    @ManyToOne
    private Ticket ticket;


    //GETTERS and SETTERS

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate( LocalDateTime date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
