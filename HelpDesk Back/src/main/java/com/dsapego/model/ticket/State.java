package com.dsapego.model.ticket;




public enum State {
    DRAFT,
    NEW,
    APPROVED,
    DECLINED,
    IN_PROGRESS,
    DONE,
    CANCELED
}
