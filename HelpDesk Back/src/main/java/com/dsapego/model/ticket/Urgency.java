package com.dsapego.model.ticket;

public enum Urgency {
    CRITICAL,
    HIGH,
    AVERAGE,
    LOW
}
