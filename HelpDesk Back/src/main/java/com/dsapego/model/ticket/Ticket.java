package com.dsapego.model.ticket;

import com.dsapego.model.*;
import com.dsapego.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@Table (name ="tickets")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private  Long id;

    @Column(name ="name")
    private String name;

    @Column(name ="description")
    private String description;

    @Column(name ="created_on")
    private LocalDateTime createdOn;

    @Column(name ="desired_resolution_date")
    private LocalDateTime desiredResolutionDate;

    @Column (name = "state_id")
    @Enumerated(EnumType.STRING)
    private State stateId;

    @Column (name = "urgency_id")
    @Enumerated(EnumType.STRING)
    private Urgency urgencyId;



    @ManyToOne (fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn (name= "assignee_id")
    private User assigneeID;

    @ManyToOne (fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn (name= "owner_id")
    private User ownerID;

    @ManyToOne (fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn (name= "approver_id")
    private User approverID;

    @ManyToOne (fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn (name= "category_id")
    private Category categoryID;

    @OneToMany (cascade = CascadeType.ALL,
            mappedBy = "ticket", orphanRemoval = true)
        private List <Attachment> attachments = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "ticket", orphanRemoval = true)
    private List <Comment> comments = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL,
            mappedBy = "ticket", orphanRemoval = true)
    private List<History> history = new ArrayList<>();






    //GETTERS and SETTERS


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public LocalDateTime getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(LocalDateTime desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public State getStateId() {
        return stateId;
    }

    public void setStateId(State stateId) {
        this.stateId = stateId;
    }

    public Urgency getUrgencyId() {
        return urgencyId;
    }

    public void setUrgencyId(Urgency urgencyId) {
        this.urgencyId = urgencyId;
    }


    public User getAssigneeID() {
        return assigneeID;
    }

    public void setAssigneeID(User assigneeID) {
        this.assigneeID = assigneeID;
    }

    public User getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(User ownerID) {
        this.ownerID = ownerID;
    }

    public User getApproverID() {
        return approverID;
    }

    public void setApproverID(User approverID) {
        this.approverID = approverID;
    }

    public Category getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Category categoryID) {
        this.categoryID = categoryID;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List <Comment> comments) {
        this.comments = comments;
    }

    public List<History> getHistory() {return history;}

    public void setHistory(List<History> history) {this.history = history;}

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", createdOn=" + createdOn +
                ", desiredResolutionDate=" + desiredResolutionDate +
                ", stateId=" + stateId +
                ", urgencyId=" + urgencyId +
                ", assigneeID=" + assigneeID +
                ", ownerID=" + ownerID +
                ", approverID=" + approverID +
                ", categoryID=" + categoryID +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(id, ticket.id) &&
                Objects.equals(name, ticket.name) &&
                Objects.equals(description, ticket.description) &&
                Objects.equals(createdOn, ticket.createdOn) &&
                Objects.equals(desiredResolutionDate, ticket.desiredResolutionDate) &&
                stateId == ticket.stateId &&
                urgencyId == ticket.urgencyId &&
                Objects.equals(assigneeID, ticket.assigneeID) &&
                Objects.equals(ownerID, ticket.ownerID) &&
                Objects.equals(approverID, ticket.approverID) &&
                Objects.equals(categoryID, ticket.categoryID) &&
                Objects.equals(attachments, ticket.attachments) &&
                Objects.equals(comments, ticket.comments) &&
                Objects.equals(history, ticket.history);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, createdOn, desiredResolutionDate, stateId, urgencyId, assigneeID, ownerID, approverID, categoryID, attachments, comments, history);
    }
}
