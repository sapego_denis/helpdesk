package com.dsapego.model;

import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Table (name ="comments")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Long id;

    @Column (name ="text")
    private String text;

    @Column (name = "dateOfComment")
    private LocalDateTime date;

    @ManyToOne
    private User user;

    @ManyToOne
    private Ticket ticket;






    //GETTERS and SETTERS

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public  LocalDateTime getDate() {
        return date;
    }

    public void setDate( LocalDateTime date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }


}
