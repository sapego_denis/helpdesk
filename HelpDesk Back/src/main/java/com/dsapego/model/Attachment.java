package com.dsapego.model;

import com.dsapego.model.ticket.Ticket;

import javax.persistence.*;


@Entity
@Table (name ="attachments")
public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Lob
    @Column(name = "files", columnDefinition = "mediumblob")
    private byte[] blob;

    @Column(name = "name")
    private String name;

    @Column (name= "mimetype")
    private String mimetype;

    @ManyToOne
    private Ticket ticket;

    public Attachment() {

    }

    public Attachment(String name, String mimetype, byte[] blob) {
        this.blob = blob;
        this.name = name;
        this.mimetype = mimetype;
    }

    //GETTERS and SETTERS


    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getBlob() {
        return blob;
    }

    public void setBlob(byte[] blob) {
        this.blob = blob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
