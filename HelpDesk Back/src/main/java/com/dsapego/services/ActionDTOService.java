package com.dsapego.services;

import com.dsapego.dto.ActionDTO;
import com.dsapego.exceptions.NotFoundException;
import org.springframework.transaction.annotation.Transactional;

public interface ActionDTOService {

    @Transactional
    ActionDTO getAvailableActions(Long ticketId) throws NotFoundException;
}
