package com.dsapego.services;

import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.user.User;



public interface UserService {

   User getUserByEmail (String login);
   User findUserById (Long id) throws NotFoundException;

    }
