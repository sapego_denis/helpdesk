package com.dsapego.services;

import com.dsapego.dto.CommentDTO;
import com.dsapego.model.Comment;

import java.util.List;

public interface CommentService {
    void create (Comment comment);
    void edit (Comment comment);
    void delite (Comment comment);
   List<Comment> findAll();
   List <Comment> getCommentForTicket(Long ticketID);

    void createNewComment(CommentDTO commentDTO);
}
