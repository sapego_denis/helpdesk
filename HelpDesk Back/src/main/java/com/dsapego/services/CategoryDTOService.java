package com.dsapego.services;

import com.dsapego.dto.CategoryDTO;

import java.util.List;

public interface CategoryDTOService {
    List<CategoryDTO> findAllCategoriesDTO();
}
