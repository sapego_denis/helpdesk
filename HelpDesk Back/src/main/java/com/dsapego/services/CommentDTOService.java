package com.dsapego.services;

import com.dsapego.dto.CommentDTO;

import java.util.List;

public interface CommentDTOService {
    List <CommentDTO> findCommentsForTicket (Long ticketID);

}
