package com.dsapego.services.impl;

import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.Category;
import com.dsapego.repository.CategoryRepository;
import com.dsapego.services.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository; }

    @Override
    @Transactional
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    @Transactional
    public void create(Category category) {categoryRepository.save(category);

    }

    @Override
    @Transactional
    public void edit(Category category) {categoryRepository.update(category);

    }

    @Override
    @Transactional
    public void delite(Category category) {categoryRepository.delete(category);

    }

    @Override
    @Transactional (readOnly = true)
    public Category findCategoryByID(Long id) throws NotFoundException {
        Optional category = categoryRepository.getByKey(id);
        if (category.isPresent()){
            return (Category)category.get();
        } else {throw new NotFoundException("category with id:"+id+" not found");

        }
    }

    }
