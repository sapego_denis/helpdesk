package com.dsapego.services.impl;

import com.dsapego.exceptions.NotFoundException;
import com.dsapego.exceptions.UploadFileException;
import com.dsapego.model.Attachment;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.repository.AttachmentRepository;
import com.dsapego.services.AttachmentService;
import com.dsapego.services.HistoryService;
import com.dsapego.services.TicketService;
import com.dsapego.services.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@Service
public class AttachmentServiceImpl implements AttachmentService {


    private final TicketService ticketService;
    private final AttachmentRepository attachmentRepository;
    private final HistoryService historyService;
    private final UserDetailService userDetailService;
    private static final Logger logger = LoggerFactory.getLogger(AttachmentServiceImpl.class);


    public AttachmentServiceImpl(TicketService ticketService, AttachmentRepository attachmentRepository, HistoryService historyService, UserDetailService userDetailService) {
        this.ticketService = ticketService;
        this.attachmentRepository = attachmentRepository;
        this.historyService = historyService;
        this.userDetailService = userDetailService;
    }

    @Override
    public List<Attachment> findAll() {return attachmentRepository.findAll();
    }

    @Override
    public void create(Attachment attachment) {attachmentRepository.save(attachment);
    }

    @Override
    @Transactional
    public void create(MultipartFile file, Long id) throws UploadFileException {
        try {
        Attachment attachment = new Attachment();
        attachment.setMimetype(file.getContentType());
        attachment.setName(file.getOriginalFilename());
        attachment.setBlob(file.getBytes());
        Ticket ticket = ticketService.findTicketByID(id);
        attachment.setTicket(ticket);
        attachmentRepository.save(attachment);
        historyService.createStoryFileIsAttached(ticket, file.getOriginalFilename(), userDetailService.getCurrentUser());

            logger.debug("File uploaded successfully! ->filename = {}", file.getOriginalFilename());
        } catch (Exception e){
           if (e instanceof MaxUploadSizeExceededException){
               throw new UploadFileException ("You have uploaded the file before or the file's size > 500KB", e);
           }
            logger.debug("Fail! Maybe you had uploaded the file before or the file's size > 500KB");
        }
    }

    @Override
    @Transactional
    public void edit(Attachment attachment) {attachmentRepository.update(attachment);

    }

    @Override
    @Transactional
    public Attachment findByID(Long id) throws NotFoundException {
        Optional attachment =attachmentRepository.getByKey(id);
        if (attachment.isPresent()) {
            return (Attachment) attachment.get();
        } else {
            throw new NotFoundException("attachment with id:" + id + " not found");
        }
    }

    @Override
    @Transactional
    public List<Attachment> findByTicketID(Long ticketID) {
        return attachmentRepository.getAttachmentsByTicketID(ticketID);
    }
}
