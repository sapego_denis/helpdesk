package com.dsapego.services.impl;

import com.dsapego.dto.CommentDTO;
import com.dsapego.dto.TicketDTO;
import com.dsapego.convertor.TicketsDTOtoTicketsConverter;
import com.dsapego.convertor.TicketsToTicketsDTOConverter;
import com.dsapego.exceptions.MailTerminalExeption;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.services.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@Service
public class TicketDTOServiceImpl implements TicketDTOService {


    private final TicketsToTicketsDTOConverter toTicketsDTOConverter;
    private final TicketsDTOtoTicketsConverter toTicketsConverter;
    private final TicketService ticketService;
    private final FeedbackService feedbackService;
    private final UserDetailService userDetailService;
    private final CommentService commentService;



    public TicketDTOServiceImpl(TicketsToTicketsDTOConverter toTicketsDTOConverter, TicketsDTOtoTicketsConverter toTicketsConverter, TicketService ticketService, FeedbackService feedbackService, UserDetailService userDetailService, CommentService commentService) {
        this.toTicketsDTOConverter = toTicketsDTOConverter;
        this.toTicketsConverter = toTicketsConverter;
        this.ticketService = ticketService;
        this.feedbackService = feedbackService;
        this.userDetailService = userDetailService;
        this.commentService = commentService;
    }


    @Override
    @Transactional (readOnly = true)
    public List<TicketDTO> findTicketsDTO() {
        return toTicketsDTOConverter.convert(ticketService.findAll(), ArrayList::new);
    }

    @Override
    @Transactional (readOnly = true)
    public List<TicketDTO> findAllTicketsDTOAccordingToRole() {
        return toTicketsDTOConverter.convert(ticketService.findTicketsAccordingToRole(), ArrayList::new);
    }

    @Override
    @Transactional (readOnly = true)
    public TicketDTO findTicketDTOByID(Long id) throws NotFoundException {
        Ticket ticket = ticketService.findTicketByID(id);
        boolean feedbackIsAvailable = feedbackService.feedbackIsAvailable(ticket);
        TicketDTO ticketDTO = toTicketsDTOConverter.convert(ticket);
        ticketDTO.setFeedbackIsAvailable(feedbackIsAvailable);
        return ticketDTO;
    }

    @Override
    @Transactional (readOnly = true)
    public List <TicketDTO> findTicketsDTOofCurrentUser(){
      return toTicketsDTOConverter.convert (ticketService.findTicketByCurrentUser(), ArrayList::new);

    }

    @Override
    @Transactional
    public void editTicket(TicketDTO dto) throws MailTerminalExeption {
        if (!dto.getText().equals("")) {
            CommentDTO commentDTO= new CommentDTO();
            commentDTO.setDateOfComment(LocalDateTime.now());
            commentDTO.setTicketId(dto.getId());
            commentDTO.setUserId(userDetailService.getCurrentUser().getId());
            commentDTO.setText(dto.getText());
            commentService.createNewComment(commentDTO);
        }
        ticketService.editTicket(toTicketsConverter.convert(dto));
    }

}
