package com.dsapego.services.impl;

import com.dsapego.dto.CategoryDTO;
import com.dsapego.convertor.CategoryToCategoryDTOConverter;
import com.dsapego.services.CategoryDTOService;
import com.dsapego.services.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryDTOServiceImpl implements CategoryDTOService {

    private final CategoryToCategoryDTOConverter toCategoryDTOConverter;
    private final CategoryService categoryService;

    public CategoryDTOServiceImpl(CategoryToCategoryDTOConverter toCategoryDTOConverter, CategoryService categoryService) {
        this.toCategoryDTOConverter = toCategoryDTOConverter;
        this.categoryService = categoryService;
    }



    @Override
    @Transactional
    public List<CategoryDTO> findAllCategoriesDTO() {
        return toCategoryDTOConverter.convert(categoryService.findAll(), ArrayList::new);
    }
}
