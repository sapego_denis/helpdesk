package com.dsapego.services.impl;

import com.dsapego.dto.AttachmentDTO;
import com.dsapego.convertor.AttachmentToAttachmentDTOConverter;
import com.dsapego.services.AttachmentDTOService;
import com.dsapego.services.AttachmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AttachmentDTOServiceImpl implements AttachmentDTOService {

    private final AttachmentToAttachmentDTOConverter converter;
    private final AttachmentService attachmentService;

    public AttachmentDTOServiceImpl(AttachmentToAttachmentDTOConverter converter, AttachmentService attachmentService) {
        this.converter = converter;
        this.attachmentService = attachmentService;
    }


    @Override
    @Transactional
    public List<AttachmentDTO> getTicketAttachment(Long ticketID) {
        return converter.convert(attachmentService.findByTicketID(ticketID));
    }
}
