package com.dsapego.services.impl;

import com.dsapego.exceptions.MailTerminalExeption;
import com.dsapego.model.user.Role;
import com.dsapego.model.ticket.State;
import com.dsapego.model.Feedback;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.repository.FeedbackRepository;
import com.dsapego.services.FeedbackService;
import com.dsapego.services.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;
    private final UserDetailService userDetailService;
    private final MailServiceImpl mailService;
    private static final Logger logger = LoggerFactory.getLogger(FeedbackServiceImpl.class);



    public FeedbackServiceImpl(FeedbackRepository feedbackRepository, UserDetailService userDetailService, MailServiceImpl mailService) {
        this.feedbackRepository = feedbackRepository;
        this.userDetailService = userDetailService;
        this.mailService = mailService;
    }

    @Override
    @Transactional
    public void create(Feedback feedback) { feedbackRepository.save(feedback);

    }

    @Override
    @Transactional
    public void edit(Feedback feedback) { feedbackRepository.update(feedback);

    }

    @Override
    @Transactional
    public void delite(Feedback feedback) {feedbackRepository.delete(feedback);

    }

    @Override
    @Transactional
    public List<Feedback> findAll() {return feedbackRepository.findAll(); }

    @Override
    @Transactional
    public boolean feedbackIsAvailable(Ticket ticket) {
        boolean feedbackIsAvailable;
        Role role =userDetailService.getCurrentUser().getRole();
        feedbackIsAvailable = ticket.getStateId().equals(State.DONE) && (role.equals(Role.MANAGER) | role.equals(Role.EMPLOYEE));
        logger.info("Leaving of feedback is available for current user");
        return feedbackIsAvailable;
    }

    @Override
    public void createFeedback(Feedback feedback) throws MailTerminalExeption {
        try {
        mailService.sendFeedbackWasProvidedTemplate(feedback.getTicket());}
        catch (Exception ex){
            logger.debug("SMPT terminal doesn't work. Please run mailhog");
            throw new MailTerminalExeption("SMPT terminal doesn't work", ex);
        }
        feedbackRepository.save(feedback);
        logger.info("Feedback was provided");
    }
}
