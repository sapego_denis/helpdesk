package com.dsapego.services.impl;


import com.dsapego.model.History;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;
import com.dsapego.repository.HistoryRepository;
import com.dsapego.services.HistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
@Service
public class HistoryServiceImpl implements HistoryService {

    private static final Logger logger = LoggerFactory.getLogger(HistoryServiceImpl.class);

    private HistoryRepository historyRepository;

    public HistoryServiceImpl(HistoryRepository historyRepository) {
        this.historyRepository = historyRepository;

    }



    @Override
    @Transactional
    public void create(History history) {historyRepository.save(history);
        }

    @Override
    @Transactional
    public void edit(History history) {historyRepository.update(history);

    }

    @Override
    @Transactional
    public void delite(History history) {historyRepository.delete(history);

    }

    @Override
    public List<History> findAll() {
        return historyRepository.findAll();
    }

    @Override
    @Transactional
    public List <History> getHistoryForTicket(Long ticketID) { return historyRepository.getHistoryForTicket(ticketID);   }

    @Override
    @Transactional (readOnly = true)
    public void createNewTicketStory(Ticket ticket, User user) {
        History history = new History();
        history.setDate(LocalDateTime.now());
        history.setDescription("Ticket is created");
        history.setAction("Ticket is created");
        history.setUser(user);
        history.setTicket(ticket);
        historyRepository.save(history);
        logger.debug("Was created new story of ticket creation");


    }

    @Override
    @Transactional (readOnly = true)
    public void createStoryStatusChanged(Ticket ticket, String oldStatus, User user) {
        String newStatus = ticket.getStateId().name();
        History history = new History();
        history.setDate(LocalDateTime.now());
        history.setDescription("Ticket status is changed");
        history.setAction("Ticket status is changed from "+oldStatus+"  to  "+ newStatus);
        history.setTicket(ticket);
        history.setUser(user);
        historyRepository.save(history);
        logger.debug("Was created new story of ticket status changing");
    }

    @Override
    @Transactional (readOnly = true)
    public History createStoryTicketEdited(Ticket ticket, User user) {
        History history = new History();
        history.setDate(LocalDateTime.now());
        history.setDescription("Ticket edited");
        history.setAction("Ticket edited");
        history.setTicket(ticket);
        history.setUser(user);
        historyRepository.save(history);
        logger.debug("Was created new story of ticket editing");
        return history;
    }

    @Override
    @Transactional (readOnly = true)
    public void createStoryFileIsAttached(Ticket ticket, String originalFilename, User user) {
        History history = new History();
        history.setDate(LocalDateTime.now());
        history.setDescription("File is attached");
        history.setAction("File "+ originalFilename + " is attached");
        history.setTicket(ticket);
        history.setUser(user);
        historyRepository.save(history);
        logger.debug("Was created new story of attaching file to ticket");
    }
}
