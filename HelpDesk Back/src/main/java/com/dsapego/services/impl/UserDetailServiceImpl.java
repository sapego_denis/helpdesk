package com.dsapego.services.impl;

import com.dsapego.model.user.User;
import com.dsapego.repository.UserRepository;
import com.dsapego.security.jwt.JwtUser;
import com.dsapego.services.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
public class UserDetailServiceImpl implements UserDetailService {
    private static final Logger logger = LoggerFactory.getLogger(UserDetailServiceImpl.class);


    private final UserRepository userRepository;

    public UserDetailServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JwtUser principal = (JwtUser) auth.getPrincipal();
        Optional user = userRepository.getByKey(principal.getId());
        User currentUser = null;
        if (user.isPresent()){
           currentUser = (User) user.get();
            logger.debug("Current user {}",  user);
        }
        return currentUser;
    }
}
