package com.dsapego.services.impl;

import com.dsapego.dto.CommentDTO;
import com.dsapego.convertor.CommentsToCommentsDTOConverter;
import com.dsapego.services.CommentDTOService;
import com.dsapego.services.CommentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CommentDTOServiceImpl implements CommentDTOService {
    private final CommentsToCommentsDTOConverter converter;
    private final CommentService commentService;

    public CommentDTOServiceImpl(CommentsToCommentsDTOConverter converter, CommentService commentService) {
        this.converter = converter;
        this.commentService = commentService;
    }

    @Override
    @Transactional (readOnly = true)
    public List<CommentDTO> findCommentsForTicket(Long ticketID) {
        return converter.convert(commentService.getCommentForTicket(ticketID));
    }
}
