package com.dsapego.services.impl;

import com.dsapego.dto.FeedbackDTO;
import com.dsapego.convertor.FeedbackDTOtoFeedbackConverter;
import com.dsapego.exceptions.MailTerminalExeption;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.Feedback;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.services.FeedbackService;
import com.dsapego.services.FeedbackServiceDTO;
import com.dsapego.services.TicketService;
import com.dsapego.services.UserDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FeedbackServiceDTOImpl implements FeedbackServiceDTO {

    private final FeedbackService feedbackService;
    private final TicketService ticketService;
    private final FeedbackDTOtoFeedbackConverter dtOtoFeedbackConverter;
    private final UserDetailService userDetailService;

    public FeedbackServiceDTOImpl(FeedbackService feedbackService, TicketService ticketService, FeedbackDTOtoFeedbackConverter dtOtoFeedbackConverter, UserDetailService userDetailService) {
        this.feedbackService = feedbackService;
        this.ticketService = ticketService;
        this.dtOtoFeedbackConverter = dtOtoFeedbackConverter;
        this.userDetailService = userDetailService;
    }

    @Override
    @Transactional
    public void createFeedback(Long id, FeedbackDTO dto) throws NotFoundException, MailTerminalExeption {
        Ticket ticket = ticketService.findTicketByID(id);
        Feedback feedback = dtOtoFeedbackConverter.convert(dto);
        feedback.setTicket(ticket);
        feedback.setUser(userDetailService.getCurrentUser());
        feedbackService.createFeedback(feedback);

    }
}
