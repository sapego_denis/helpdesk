package com.dsapego.services.impl;

import com.dsapego.model.user.Role;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;
import com.dsapego.repository.UserRepository;
import com.dsapego.services.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.ArrayList;
import java.util.List;


@Service
public class MailServiceImpl implements MailService {

    private final UserRepository userRepository;
    private static final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);


    private final JavaMailSender mailSender;
    private final SpringTemplateEngine htmlTemplateEngine;



    public MailServiceImpl(UserRepository userRepository, JavaMailSender mailSender, SpringTemplateEngine htmlTemplateEngine) {
        this.userRepository = userRepository;
        this.mailSender = mailSender;
        this.htmlTemplateEngine = htmlTemplateEngine;
    }

    @Override
    public void send(User user, String templateNumber, String subject, Ticket ticket) throws MessagingException {
        try {

            final Context ctx = new Context();

            ctx.setVariable("ticketName", ticket.getName());
            ctx.setVariable("ticketId", ticket.getId());
            ctx.setVariable("firstName", ticket.getOwnerID().getFirstName());
            ctx.setVariable("lastName", ticket.getOwnerID().getLastName());

            final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
            final String htmlContent = this.htmlTemplateEngine.process(templateNumber, ctx);

            message.setText(htmlContent, true);
            message.setSubject(subject);
            message.setFrom("support@helpdesk.com");
            message.setTo(user.getEmail());

            this.mailSender.send(mimeMessage);
            logger.info("Email sent");

        } catch (MessagingException ex) {
            logger.info("Email didn't sent");
            throw new MessagingException("Email didn't sent", ex);
        }
    }

    @Override
    public void sendNewTicketForApprovalTemplate(Ticket ticket) throws MessagingException {

        List<User> recipients = userRepository.getAllUsersByRole(Role.MANAGER);
        String subject ="New ticket for approval";
        for (User recipient: recipients) {
            send(recipient,"newTicketForApprovalTemplate", subject, ticket);
        }
    }

    @Override
    public void sendTicketWasApprovedTemplate(Ticket ticket) throws MessagingException {
        List<User> recipients = new ArrayList<>();
        List <User> allEngineer = userRepository.getAllUsersByRole(Role.ENGINEER);
        User creator = ticket.getOwnerID();
        recipients.add(creator);
        recipients.addAll(allEngineer);
        String subject ="Ticket was approved";
        for (User recipient: recipients) {
            send(recipient,"ticketWasApprovedTemplate", subject, ticket);
        }
    }

    @Override
    public void sendTicketWasDeclinedTemplate(Ticket ticket) throws MessagingException {
       User recipient = ticket.getOwnerID();
       String subject ="Ticket was declined";
       send(recipient,"ticketWasDeclinedTemplate", subject, ticket);

    }

    @Override
    public void sendTicketWasCancelledByManagerTemplate(Ticket ticket) throws MessagingException {
        User recipient = ticket.getOwnerID();
        String subject ="Ticket was cancelled";
        send(recipient,"ticketWasCancelledByManagerTemplate", subject, ticket);
    }

    @Override
    public void sendTicketWasCancelledByEngineerTemplate(Ticket ticket) throws MessagingException {
        User recipientApprover = ticket.getApproverID();
        User recipientCreator = ticket.getOwnerID();
        String subject ="Ticket was cancelled";
        send(recipientApprover,"ticketWasCancelledByEngineerTemplate", subject, ticket);
        send(recipientCreator,"ticketWasCancelledByEngineerTemplate", subject, ticket);

    }

    @Override
    public void sendTicketWasDoneTemplate(Ticket ticket) throws MessagingException {
        User recipient = ticket.getOwnerID();
        String subject ="Ticket was done";
        send(recipient,"ticketWasDoneTemplate", subject, ticket);
    }

    @Override
    public void sendFeedbackWasProvidedTemplate(Ticket ticket) throws MessagingException {
        User recipient = ticket.getOwnerID();
        String subject ="Feedback was provided";
        send(recipient,"feedbackWasProvidedTemplate", subject, ticket);
    }

    @Override
    public void sendEmail(String template, Ticket ticket) throws MessagingException {
       if (template.equals("newTicketForApprovalTemplate")){ //#1
           sendNewTicketForApprovalTemplate(ticket);
       }
        if (template.equals("ticketWasApprovedTemplate")){ //#2
            sendTicketWasApprovedTemplate(ticket);
        }
        if (template.equals("ticketWasDeclinedTemplate")){ //#3
            sendTicketWasDeclinedTemplate(ticket);
        }
        if (template.equals("ticketWasCancelledByManagerTemplate")){ //#4
            sendTicketWasCancelledByManagerTemplate(ticket);
        }
        if (template.equals("ticketWasCancelledByEngineerTemplate")){ //#5
            sendTicketWasCancelledByEngineerTemplate(ticket);
        }
        if (template.equals("ticketWasDoneTemplate")){ //#6
            sendTicketWasDoneTemplate(ticket);
        }
        if (template.equals("feedbackWasProvidedTemplate")){ //#7
            sendFeedbackWasProvidedTemplate(ticket);
        }

    }
}
