package com.dsapego.services.impl;

import com.dsapego.dto.CommentDTO;
import com.dsapego.dto.TicketDTO;
import com.dsapego.convertor.TicketsDTOtoTicketsConverter;
import com.dsapego.exceptions.MailTerminalExeption;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.user.Role;
import com.dsapego.model.ticket.State;
import com.dsapego.model.History;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;
import com.dsapego.repository.TicketRepository;
import com.dsapego.services.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Service
public class TicketServiceImpl implements TicketService {

    private static final Logger logger;

    static {
        logger = LoggerFactory.getLogger(TicketServiceImpl.class);
    }

    private final TicketRepository ticketRepository;
    private final TicketsDTOtoTicketsConverter ticketsDTOtoTicketsConverter;
    private final UserDetailService userDetailService;
    private final HistoryService historyService;
    private final CommentService commentService;
    private final MailServiceImpl mailService;
    private String mailTemplate;

    public TicketServiceImpl(TicketRepository ticketRepository,
                             TicketsDTOtoTicketsConverter ticketsDTOtoTicketsConverter,
                             UserDetailService userDetailService,
                             HistoryService historyService,
                             CommentService commentService,
                             MailServiceImpl mailService)
    {
        this.ticketRepository = ticketRepository;
        this.ticketsDTOtoTicketsConverter = ticketsDTOtoTicketsConverter;
        this.userDetailService = userDetailService;
        this.historyService = historyService;
        this.commentService = commentService;
        this.mailService = mailService;
    }

    public static final String TERMINAL_OFF = "SMPT terminal doesn't work";

    State newState;


    @Override
    @Transactional (readOnly = true)
    public Long createNewTicket(TicketDTO ticketDTO)  {
        User owner = userDetailService.getCurrentUser();
        ticketDTO.setOwnerId(userDetailService.getCurrentUser().getId().toString());
        Ticket ticket = ticketsDTOtoTicketsConverter.convert(ticketDTO);
        ticketRepository.save(ticket);
        historyService.createNewTicketStory(ticket, owner);
        if (ticketDTO.getState() ==State.NEW){
            try {
                mailService.sendNewTicketForApprovalTemplate(ticket);}
            catch (Exception ex){
                logger.debug(TERMINAL_OFF);
            }
            }


        if (!ticketDTO.getText().equals("")) {
        CommentDTO commentDTO= new CommentDTO();
        commentDTO.setDateOfComment(LocalDateTime.now());
        commentDTO.setTicketId(ticket.getId());
        commentDTO.setUserId(owner.getId());
        commentDTO.setText(ticketDTO.getText());
        commentService.createNewComment(commentDTO);
        }
        logger.debug("Ticket with id"+ticket.getId()+ " created");
        return ticket.getId();
    }

    @Override
    @Transactional (readOnly = true)
    public List<Ticket> findTicketsAccordingToRole() {
        final User currentUser = userDetailService.getCurrentUser();
        final Role role = currentUser.getRole();
        if (role == Role.MANAGER) {
            return
                    ticketRepository.findAllTicketsForManager(currentUser);
        } else if (role == Role.ENGINEER) {
            return
                    ticketRepository.findAllTicketsForEngineer(currentUser);
        } else if (role == Role.EMPLOYEE) {
            return
                    ticketRepository.findAllTicketsForEmployee(currentUser);
        }
        return
                ticketRepository.findAll();
    }

    @Override
    @Transactional (readOnly = true)
    public List<Ticket> findAll() {
        return ticketRepository.findAll();
    }

    @Override
    @Transactional (readOnly = true)
    public Ticket findTicketByID(Long id) throws NotFoundException {
        Optional optionalTicket = ticketRepository.getByKey(id);
        if (optionalTicket.isPresent()){
            return (Ticket) optionalTicket.get();
        } else {throw new NotFoundException("ticket with id:"+id+" not found");

        }
    }

    @Override
    @Transactional (readOnly = true)
    public List <Ticket> findTicketByCurrentUser() {
        final User currentUser = userDetailService.getCurrentUser();
        return ticketRepository.findAllByOwnerId(currentUser);
    }

    @Override
    @Transactional
    public void editTicket(Ticket ticket) throws MailTerminalExeption {
       History history = historyService.createStoryTicketEdited(ticket, userDetailService.getCurrentUser());
        ticket.getHistory().add(history);
        if (ticket.getStateId() ==State.NEW){
            try {
                  mailService.sendNewTicketForApprovalTemplate(ticket);}
            catch (Exception ex){
                logger.debug("Fail! Maybe you had uploaded the file before or the file's size > 500KB");
                throw new MailTerminalExeption("SMPT terminal doesn't work", ex);

            }
                   logger.debug(TERMINAL_OFF);
               }
        ticketRepository.update(ticket);
        logger.debug("Ticket edited");

    }

    @Override
    @Transactional (readOnly = true)
    public boolean deleteTicket(Ticket ticket) {
        ticketRepository.delete(ticket);
        logger.debug("Ticket deleted");
        return true;
    }

    @Override
    @Transactional
    public Ticket changeTicketStatus (Long ticketId, String action) throws NotFoundException {
        String doAction = action.toUpperCase();
        User user = userDetailService.getCurrentUser();
        Ticket ticket = findTicketByID(ticketId);

        if (doAction.equals("SUBMIT")) {
             newState = State.NEW;
             mailTemplate="newTicketForApprovalTemplate";
        }

        if (doAction.equals("CREATE")) {
            newState = State.DRAFT;
        }

        if (doAction.equals("APPROVE")) {
            newState = State.APPROVED;
            mailTemplate="ticketWasApprovedTemplate";
            ticket.setApproverID(user);
        }

        if (doAction.equals("DECLINE")) {
            newState = State.DECLINED;
            mailTemplate="ticketWasDeclinedTemplate";
        }

        if (doAction.equals("CANCEL")) {
            newState = State.CANCELED;
            if(ticket.getStateId().name().equals(State.APPROVED)){
                mailTemplate="ticketWasCancelledByEngineerTemplate";
            }
            else {
                mailTemplate="ticketWasCancelledByManagerTemplate";
            }
        }

        if (doAction.equals("ASSIGN TO ME")) {
            newState = State.IN_PROGRESS;
            ticket.setAssigneeID(user);
        }
        else {
            newState = State.DONE;
            mailTemplate="ticketWasDoneTemplate";
        }
        try {
            mailService.sendEmail(mailTemplate, ticket);
        }catch (Exception ex) {
            logger.debug(TERMINAL_OFF);
        }

        historyService.createStoryStatusChanged(ticket, ticket.getStateId().name(), user);
        ticketRepository.update(ticket);
        logger.debug("Ticket with id "+ticket.getId()+" updated. Status changed");
        return ticket;
    }


   }
