package com.dsapego.services.impl;

import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.user.User;
import com.dsapego.repository.UserRepository;
import com.dsapego.services.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
public class UserServiceImpl implements UserService {


    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public User getUserByEmail(String email) {
        Optional user = userRepository.getUserByEmail(email);
        User userByEmail = null;
        if (user.isPresent()) {
            userByEmail = (User) user.get();
        }
        return userByEmail;
    }

    @Override
    @Transactional (readOnly = true)
    public User findUserById(Long id) throws NotFoundException {
        Optional user = userRepository.getByKey(id);
        if (user.isPresent()) {
            return (User) user.get();
        } else {
            throw new NotFoundException("user with id:" + id + " not found");

        }
    }
}

