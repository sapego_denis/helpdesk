package com.dsapego.services.impl;

import com.dsapego.dto.HistoryDTO;
import com.dsapego.convertor.HistoryToHistoryDTOConverter;
import com.dsapego.services.HistoryDTOService;
import com.dsapego.services.HistoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class HistoryDTOServiceImpl implements HistoryDTOService {

        private final HistoryToHistoryDTOConverter converter;
        private final HistoryService historyService;

    public HistoryDTOServiceImpl(HistoryToHistoryDTOConverter converter, HistoryService historyService) {
        this.converter = converter;
        this.historyService = historyService;
    }


    @Override
        @Transactional(readOnly = true)
        public List<HistoryDTO> findHistoryForTicket(Long ticketID) {
            return converter.convert(historyService.getHistoryForTicket(ticketID));
        }
}
