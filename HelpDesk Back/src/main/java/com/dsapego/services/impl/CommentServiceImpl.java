package com.dsapego.services.impl;

import com.dsapego.dto.CommentDTO;
import com.dsapego.convertor.CommentDTOtoCommentsConverter;
import com.dsapego.model.Comment;
import com.dsapego.repository.CommentRepository;
import com.dsapego.services.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class CommentServiceImpl implements CommentService {
    private static final Logger logger = LoggerFactory.getLogger(CommentServiceImpl.class);
    private final CommentRepository commentRepository;
    private final CommentDTOtoCommentsConverter commentDTOtoCommentsConverter;
    public CommentServiceImpl(CommentRepository commentRepository, CommentDTOtoCommentsConverter commentDTOtoCommentsConverter) {
        this.commentRepository = commentRepository;
        this.commentDTOtoCommentsConverter = commentDTOtoCommentsConverter;
    }

    @Override
    public List<Comment> findAll() {
        return commentRepository.findAll();
    }

    @Override
    @Transactional
    public void create(Comment comment) {
        commentRepository.save(comment);

    }

    @Override
    @Transactional
    public void edit(Comment comment) {commentRepository.update(comment);

    }

    @Override
    @Transactional
    public void delite(Comment comment) {commentRepository.delete(comment);

    }

    @Override
    @Transactional
    public List <Comment> getCommentForTicket(Long ticketID) { return commentRepository.getCommentsForTicket(ticketID);   }

    @Override
    @Transactional (readOnly = true)
    public void createNewComment(CommentDTO commentDTO) {
        Comment comment = commentDTOtoCommentsConverter.convert(commentDTO);
        commentRepository.save(comment);
        logger.info("New comment was created");
    }

}
