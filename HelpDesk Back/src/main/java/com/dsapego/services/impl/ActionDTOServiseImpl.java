package com.dsapego.services.impl;

import com.dsapego.dto.ActionDTO;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.user.Role;
import com.dsapego.model.ticket.State;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.services.ActionDTOService;
import com.dsapego.services.TicketService;
import com.dsapego.services.UserDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class ActionDTOServiseImpl implements ActionDTOService {

    private final TicketService ticketicketService;
    private final UserDetailService userDetailService;

    public ActionDTOServiseImpl(TicketService ticketService, UserDetailService userDetailService) {
        this.ticketicketService = ticketService;
        this.userDetailService = userDetailService;
    }

private static final String SUBMIT  = "Submit";
private static final String CANCEL  = "Cancel";
private static final String DONE  = "Done";
private static final String APPROVED  = "Approved";
private static final String DECLINED  = "Declined";

    @Override
    @Transactional
    public ActionDTO getAvailableActions(Long ticketId) throws NotFoundException {
        Ticket ticket = ticketicketService.findTicketByID(ticketId);
        State currentState = ticket.getStateId();
        ActionDTO actionDTO= new ActionDTO();


        Set<String> availableActions =new HashSet<>();

        Role role =userDetailService.getCurrentUser().getRole();

        if (role.equals(Role.MANAGER)) {

            if (currentState.name().equals(State.DRAFT.toString())) {
                availableActions.add(SUBMIT);
                availableActions.add(CANCEL);
            }
            if (currentState.name().equals(State.NEW.toString())) {
                availableActions.add(APPROVED);
                availableActions.add(DECLINED);
                availableActions.add(CANCEL);
            }
            if (currentState.name().equals(State.DECLINED.toString())) {
                availableActions.add(SUBMIT);
                availableActions.add(CANCEL);
            }
        } else {
        if (role.equals(Role.ENGINEER)) {

            if (currentState.name().equals(State.APPROVED.toString())) {
                availableActions.add("Assign to Me");
                availableActions.add(CANCEL);
            }
            if (currentState.name().equals(State.IN_PROGRESS.toString())) {
                availableActions.add(DONE);
            }
        } else {
            if (currentState.name().equals(State.DRAFT.toString())) {
                availableActions.add(SUBMIT);
                availableActions.add(CANCEL);
            }
            if (currentState.name().equals(State.DECLINED.toString())) {
                availableActions.add(SUBMIT);
                availableActions.add(CANCEL);
            }
        }

        }

        actionDTO.setDoAction(availableActions);

        return actionDTO;
    }
}
