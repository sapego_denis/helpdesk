package com.dsapego.services;

import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;

import javax.mail.MessagingException;

public interface MailService {
    void send(User user, String templateNumber, String subject, Ticket ticket) throws MessagingException;

    void sendNewTicketForApprovalTemplate(Ticket ticket) throws MessagingException;

    void sendTicketWasApprovedTemplate(Ticket ticket) throws MessagingException;

    void sendTicketWasDeclinedTemplate(Ticket ticket) throws MessagingException;

    void sendTicketWasCancelledByManagerTemplate(Ticket ticket) throws MessagingException;

    void sendTicketWasCancelledByEngineerTemplate(Ticket ticket) throws MessagingException;

    void sendTicketWasDoneTemplate(Ticket ticket) throws MessagingException;

    void sendFeedbackWasProvidedTemplate(Ticket ticket) throws MessagingException;

    void sendEmail(String template, Ticket ticket) throws MessagingException;
}
