package com.dsapego.services;

import com.dsapego.model.History;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;

import java.util.List;

public interface HistoryService {
    void create (History history);
    void edit (History history);
    void delite (History history);
    List <History> findAll();

    List <History> getHistoryForTicket(Long ticketID);

    void createNewTicketStory(Ticket ticket, User owner);

    void createStoryStatusChanged(Ticket ticket, String action, User user);

    History createStoryTicketEdited(Ticket ticket, User currentUser);

    void createStoryFileIsAttached(Ticket ticket, String originalFilename, User user);
}
