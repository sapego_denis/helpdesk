package com.dsapego.services;

import com.dsapego.dto.FeedbackDTO;
import com.dsapego.exceptions.MailTerminalExeption;
import com.dsapego.exceptions.NotFoundException;

public interface FeedbackServiceDTO {
    void createFeedback(Long id, FeedbackDTO dto) throws NotFoundException, MailTerminalExeption;
}
