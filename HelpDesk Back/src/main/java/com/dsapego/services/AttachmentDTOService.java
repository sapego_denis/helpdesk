package com.dsapego.services;

import com.dsapego.dto.AttachmentDTO;

import java.util.List;

public interface AttachmentDTOService {
    List<AttachmentDTO> getTicketAttachment(Long ticketID);
}
