package com.dsapego.services;

import com.dsapego.dto.TicketDTO;
import com.dsapego.exceptions.MailTerminalExeption;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.ticket.Ticket;

import java.util.List;

public interface TicketService {

    Long createNewTicket (TicketDTO ticketdto) throws NotFoundException;
    List<Ticket> findAll();
    List<Ticket> findTicketsAccordingToRole();
    Ticket findTicketByID (Long id) throws NotFoundException;

    List <Ticket> findTicketByCurrentUser();

    void editTicket (Ticket ticket) throws MailTerminalExeption;
    boolean deleteTicket(Ticket ticket);

    Ticket changeTicketStatus(Long ticketId, String action) throws NotFoundException;
}



