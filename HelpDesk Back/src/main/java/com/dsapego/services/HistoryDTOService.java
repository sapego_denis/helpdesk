package com.dsapego.services;

import com.dsapego.dto.HistoryDTO;

import java.util.List;

public interface HistoryDTOService {
    List<HistoryDTO> findHistoryForTicket (Long ticketID);
}
