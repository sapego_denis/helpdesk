package com.dsapego.services;

import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.Category;

import java.util.List;

public interface CategoryService {
    void create (Category category);
    void edit (Category category);
    void delite (Category category);
    List<Category> findAll();
    Category findCategoryByID (Long id) throws NotFoundException;
}
