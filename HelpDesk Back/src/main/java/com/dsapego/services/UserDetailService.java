package com.dsapego.services;

import com.dsapego.model.user.User;


public interface UserDetailService {
    User getCurrentUser ();
}
