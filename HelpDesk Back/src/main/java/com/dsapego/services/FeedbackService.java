package com.dsapego.services;

import com.dsapego.exceptions.MailTerminalExeption;
import com.dsapego.model.Feedback;
import com.dsapego.model.ticket.Ticket;

import java.util.List;

public interface FeedbackService {
    void create (Feedback feedback);
    void edit (Feedback feedback);
    void delite (Feedback feedback);
    List<Feedback> findAll();
    boolean feedbackIsAvailable(Ticket ticket);

    void createFeedback(Feedback feedback) throws MailTerminalExeption;
}
