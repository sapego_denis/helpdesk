package com.dsapego.services;

import com.dsapego.exceptions.NotFoundException;
import com.dsapego.exceptions.UploadFileException;
import com.dsapego.model.Attachment;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface AttachmentService {
    void create (Attachment attachment);
    Attachment findByID (Long id) throws NotFoundException;

    @Transactional
    void create(MultipartFile file, Long id) throws UploadFileException;

    void edit (Attachment attachment);
    List<Attachment> findAll();
    List<Attachment> findByTicketID(Long ticketID);
}
