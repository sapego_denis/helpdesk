package com.dsapego.services;

import com.dsapego.dto.TicketDTO;
import com.dsapego.exceptions.MailTerminalExeption;
import com.dsapego.exceptions.NotFoundException;

import java.util.List;


public interface TicketDTOService {
    List<TicketDTO> findAllTicketsDTOAccordingToRole();
    List <TicketDTO> findTicketsDTO();
    TicketDTO findTicketDTOByID(Long id) throws NotFoundException;
    List <TicketDTO> findTicketsDTOofCurrentUser();

    void editTicket(TicketDTO dto) throws MailTerminalExeption;

}
