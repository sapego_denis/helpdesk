package com.dsapego.exceptions;

public class UploadFileException extends Exception {
    public UploadFileException() {
        super();
    }

    public UploadFileException(String message) {
        super(message);
    }

    public UploadFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public UploadFileException(Throwable cause) {
        super(cause);
    }
}
