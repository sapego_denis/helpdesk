package com.dsapego.exceptions;

public class MailTerminalExeption extends Exception {
    public MailTerminalExeption() {
        super();
    }

    public MailTerminalExeption(String message) {
        super(message);
    }

    public MailTerminalExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public MailTerminalExeption(Throwable cause) {
        super(cause);
    }
}
