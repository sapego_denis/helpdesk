package com.dsapego.convertor;

import com.dsapego.dto.UserDTO;
import com.dsapego.model.user.User;
import org.springframework.stereotype.Component;

@Component
public class UserDTOConverter {


    public static UserDTO convert (User source) {
        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(source.getFirstName());
        userDTO.setLastName(source.getLastName());
        userDTO.setRole(source.getRole().toString());
        userDTO.setEmail(source.getEmail());
        userDTO.setPassword(source.getPassword());
        userDTO.setUsername(source.getEmail());

        return userDTO;
    }

}
