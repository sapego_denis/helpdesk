package com.dsapego.convertor;

import com.dsapego.dto.TicketDTO;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.services.ActionDTOService;
import org.springframework.stereotype.Component;

@Component
public class TicketsToTicketsDTOConverter implements Converter<Ticket, TicketDTO> {

    private final ActionDTOService actionDTOService;
    private final AttachmentToAttachmentDTOConverter converter;

    public TicketsToTicketsDTOConverter(ActionDTOService actionDTOService, AttachmentToAttachmentDTOConverter converter) {
        this.actionDTOService = actionDTOService;
        this.converter = converter;
    }


    @Override
    public TicketDTO convert(Ticket source) {
        final TicketDTO ticketDTO = new TicketDTO();
        ticketDTO.setId(source.getId());
        ticketDTO.setName(source.getName());
        ticketDTO.setDescription(source.getDescription());
        ticketDTO.setCreatedOn(source.getCreatedOn());
        ticketDTO.setDesiredResolutionDate(source.getDesiredResolutionDate().toString());
        ticketDTO.setState(source.getStateId());
        ticketDTO.setUrgency(source.getUrgencyId());
        ticketDTO.setAssigneeFirstName(source.getAssigneeID() == null ? null : source.getAssigneeID().getFirstName());
        ticketDTO.setAssigneeLastName(source.getAssigneeID() == null ? null : source.getAssigneeID().getLastName());
        ticketDTO.setOwnerFirstName(source.getOwnerID() == null ? null : source.getOwnerID().getFirstName());
        ticketDTO.setOwnerLastName(source.getOwnerID() == null ? null :source.getOwnerID().getLastName());
        ticketDTO.setApproverFirstName(source.getApproverID()  == null ? null : source.getApproverID().getFirstName());
        ticketDTO.setApproverLastName(source.getApproverID()  == null ? null : source.getApproverID().getLastName());
        ticketDTO.setAttachments(source.getAttachments() == null ? null :converter.convert(source.getAttachments()));
        ticketDTO.setCategory(source.getCategoryID());
        try {
            ticketDTO.setAvailableActions(actionDTOService.getAvailableActions(source.getId()));
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        ticketDTO.setFeedbackIsAvailable(false);
        return ticketDTO;
    }


}
