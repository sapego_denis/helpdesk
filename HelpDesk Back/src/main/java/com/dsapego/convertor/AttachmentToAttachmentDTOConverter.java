package com.dsapego.convertor;

import com.dsapego.dto.AttachmentDTO;
import com.dsapego.model.Attachment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AttachmentToAttachmentDTOConverter {



    public List<AttachmentDTO> convert(List<Attachment> source) {
        List <AttachmentDTO> attachmentDTOList = new ArrayList<>();

        for (Attachment attachment: source) {
            final AttachmentDTO attachmentDTO = new AttachmentDTO();
                attachmentDTO.setId(attachment.getId());
                attachmentDTO.setName(attachment.getName());
                attachmentDTO.setMimetype(attachment.getMimetype());
                attachmentDTO.setTicketId(attachment.getTicket().getId());
                attachmentDTOList.add(attachmentDTO);

        }
        return attachmentDTOList;
    }
}
