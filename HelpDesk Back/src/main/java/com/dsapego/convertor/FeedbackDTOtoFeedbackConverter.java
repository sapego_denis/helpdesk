package com.dsapego.convertor;

import com.dsapego.dto.FeedbackDTO;
import com.dsapego.model.Feedback;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class FeedbackDTOtoFeedbackConverter implements Converter <FeedbackDTO, Feedback> {
    @Override
    public Feedback convert(FeedbackDTO source) {
        final Feedback feedback = new Feedback();
        feedback.setDate(LocalDateTime.now());

        if (source.getRate() == null) {
            feedback.setRate(null);
        } else feedback.setRate(source.getRate());

        if (source.getText() == null) {
            feedback.setText(null);
        } else feedback.setText(source.getText());

        return feedback;
    }
}
