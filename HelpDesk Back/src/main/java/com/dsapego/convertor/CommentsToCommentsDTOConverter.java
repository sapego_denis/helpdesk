package com.dsapego.convertor;

import com.dsapego.dto.CommentDTO;
import com.dsapego.model.Comment;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class CommentsToCommentsDTOConverter  {


    public List<CommentDTO> convert(List<Comment> source) {
        List <CommentDTO> commentDTOList = new ArrayList<>();
        for ( Comment comment: source) {
        final CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setDateOfComment(comment.getDate()== null ? LocalDateTime.now() : comment.getDate());
            if (comment.getText() == null) commentDTO.setText(null);
            else commentDTO.setText(comment.getText());
        commentDTO.setUserFullName(comment.getUser().getFirstName() + " "+ comment.getUser().getLastName());
        commentDTO.setUserId(comment.getUser().getId());
        commentDTO.setTicketId(comment.getTicket().getId());
        commentDTOList.add(commentDTO);
        }
        return commentDTOList;
    }
}
