package com.dsapego.convertor;

import com.dsapego.dto.HistoryDTO;
import com.dsapego.model.History;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class HistoryToHistoryDTOConverter {
    public List<HistoryDTO> convert(List<History> source) {
        List <HistoryDTO> historyDTOList = new ArrayList<>();
        for ( History history: source) {
            final HistoryDTO historyDTO = new HistoryDTO();
            historyDTO.setId(history.getId());
            historyDTO.setDateOfStory(history.getDate()== null ? LocalDateTime.now() : history.getDate());
            if (history.getAction() == null) historyDTO.setAction(null);
            else historyDTO.setAction(history.getAction());
            if (history.getDescription() == null) historyDTO.setDescription(null);
            else historyDTO.setDescription(history.getDescription());
            historyDTO.setUserFullName(history.getUser().getFirstName() + " "+ history.getUser().getLastName());
            historyDTO.setUserId(history.getUser().getId());
            historyDTO.setTicketId(history.getTicket().getId());
            historyDTOList.add(historyDTO);
        }
        return historyDTOList;
    }
}
