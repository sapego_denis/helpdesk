package com.dsapego.convertor;

import com.dsapego.dto.TicketDTO;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.services.CategoryService;
import com.dsapego.services.UserDetailService;
import com.dsapego.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class TicketsDTOtoTicketsConverter implements Converter<TicketDTO, Ticket> {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailService userDetailService;

    @Override
    public Ticket convert(TicketDTO source)  {
        DateTimeFormatter dTF = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        final Ticket ticket = new Ticket();
        if (source.getId() == null) ticket.setId(null);
        else ticket.setId(source.getId());
        ticket.setName(source.getName());
        if (source.getDescription() == null) ticket.setDescription(null);
        else ticket.setDescription(source.getDescription());
        ticket.setCreatedOn(source.getCreatedOn() == null ? LocalDateTime.now() : source.getCreatedOn());
        ticket.setDesiredResolutionDate( source.getDesiredResolutionDate() == null ? null: (LocalDateTime.parse(source.getDesiredResolutionDate(), dTF)));
        if (source.getState() == null) ticket.setStateId(null);
        else ticket.setStateId(source.getState());
        ticket.setUrgencyId(source.getUrgency());
        try {
            ticket.setCategoryID(categoryService.findCategoryByID(source.getCategoryNum()));
            ticket.setOwnerID(source.getOwnerId() != null ? userService.findUserById(Long.parseLong(source.getOwnerId())) :
                   userDetailService.getCurrentUser() );
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        return ticket;
    }
}
