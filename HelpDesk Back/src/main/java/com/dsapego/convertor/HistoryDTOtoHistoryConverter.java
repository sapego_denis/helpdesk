package com.dsapego.convertor;

import com.dsapego.dto.HistoryDTO;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.History;
import com.dsapego.services.TicketService;
import com.dsapego.services.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class HistoryDTOtoHistoryConverter implements Converter <HistoryDTO, History>{

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private TicketService ticketService;

    @Override
    public History convert(HistoryDTO source)  {
        final History history = new History();
        if (source.getAction() == null) history.setAction(null);
        else history.setAction(source.getAction());
        if (source.getDescription() == null) history.setDescription(null);
        else history.setDescription(source.getDescription());
        history.setDate( source.getDateOfStory() == null ? null : LocalDateTime.now());
        history.setUser(userDetailService.getCurrentUser());
        try {
            history.setTicket(ticketService.findTicketByID(source.getTicketId()));
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return history;
    }
}
