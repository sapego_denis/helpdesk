package com.dsapego.convertor;


import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public interface Converter<S,T> {

    T convert(S source);

    default <C extends Collection<T>> C convert(Collection<S> entities, Supplier<C> collectionSupplier) {
        if (entities != null) {
            return entities.stream()
                    .map(this::convert)
                    .collect(Collectors.toCollection(collectionSupplier));
        }
        return collectionSupplier.get();
    }
}