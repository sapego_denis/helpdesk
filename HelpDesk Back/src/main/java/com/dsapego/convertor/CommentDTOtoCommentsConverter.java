package com.dsapego.convertor;

import com.dsapego.dto.CommentDTO;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.Comment;
import com.dsapego.services.TicketService;
import com.dsapego.services.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CommentDTOtoCommentsConverter implements Converter <CommentDTO, Comment>{

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private TicketService ticketService;

    @Override
    public Comment convert(CommentDTO source) {
        final Comment comment = new Comment();
        if (source.getText() == null) comment.setText(null);
        else comment.setText(source.getText());
        comment.setDate( source.getDateOfComment() == null ? null : LocalDateTime.now());
        comment.setUser(userDetailService.getCurrentUser());
        try {
            comment.setTicket(ticketService.findTicketByID(source.getTicketId()));
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return comment;
    }
}
