package com.dsapego.convertor;

import com.dsapego.dto.CategoryDTO;
import com.dsapego.model.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryToCategoryDTOConverter implements Converter <Category, CategoryDTO> {

    @Override
    public CategoryDTO convert(Category source) {
        final CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(source.getId());
        categoryDTO.setName(source.getName());
        return categoryDTO;
    }
}
