package com.dsapego.security.jwt;


import com.dsapego.model.user.User;


public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(User user) {

        JwtUser jwtUser;
        jwtUser = new JwtUser(
                user.getId(),
                user.getEmail(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword()
        );
        return jwtUser;
    }

}
