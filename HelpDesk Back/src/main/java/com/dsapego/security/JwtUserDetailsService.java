package com.dsapego.security;

import com.dsapego.model.user.User;
import com.dsapego.security.jwt.JwtUser;
import com.dsapego.security.jwt.JwtUserFactory;
import com.dsapego.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service

public class JwtUserDetailsService implements UserDetailsService {
    private static final Logger log = LoggerFactory.getLogger(JwtUserDetailsService.class);
    private final UserService userService;

    @Autowired
    public JwtUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String email){
        User user = userService.getUserByEmail(email);

        if (user == null) {
            throw new UsernameNotFoundException("User with username: " + email + " not found");
        }

        JwtUser jwtUser = JwtUserFactory.create(user);
        log.debug("IN loadUserByUsername - user with username: {} successfully loaded", email);
        log.debug("Password {}", jwtUser.getPassword());
        return jwtUser;
    }
}
