package com.dsapego.dto;

import java.time.LocalDateTime;

public class HistoryDTO {

        private Long id;
        private String action;
        private String description;
        private LocalDateTime dateOfStory;
        private String userFullName;
        private Long userId;
        private Long ticketId;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDateOfStory() {
        return dateOfStory;
    }

    public void setDateOfStory(LocalDateTime dateOfStory) {
        this.dateOfStory = dateOfStory;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }
}
