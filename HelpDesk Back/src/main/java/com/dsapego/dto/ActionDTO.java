package com.dsapego.dto;

import java.util.Set;

public class ActionDTO {
    private String message;
    private Set<String> doAction;



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Set<String> getDoAction() {
        return doAction;
    }

    public void setDoAction(Set<String> doAction) {
        this.doAction = doAction;
    }
}

