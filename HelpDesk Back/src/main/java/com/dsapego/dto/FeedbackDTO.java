package com.dsapego.dto;

import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.user.User;

import java.time.LocalDateTime;

public class FeedbackDTO {

    private Long id;
    private Integer rate;
    private LocalDateTime date;
    private String text;
    private User user;
    private Ticket ticket;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }
}
