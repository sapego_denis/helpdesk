package com.dsapego.dto;


import com.dsapego.model.Category;
import com.dsapego.model.Comment;
import com.dsapego.model.ticket.State;
import com.dsapego.model.ticket.Urgency;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class TicketDTO {

    private  Long id;
    @NotNull
    @Pattern(regexp = "[a-z0-9_~!@#$%^&*)(=-+-]")
    @Length (max = 500)
    private String name;
    private String description;
    private LocalDateTime createdOn;
    private String desiredResolutionDate;
    private State state;
    @NotNull
    private Urgency urgency;
    private String assigneeFirstName;
    private String assigneeLastName;
    private String ownerId;
    private String ownerFirstName;
    private String ownerLastName;
    private String approverFirstName;
    private String approverLastName;
    @NotNull
    private Category category;
    @NotNull
    private Long categoryNum;
    private List <AttachmentDTO> attachments;
    private Set<Comment> comments;
    private String text;
    private ActionDTO availableActions;
    private boolean feedbackIsAvailable;





    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAssigneeFirstName() {
        return assigneeFirstName;
    }

    public void setAssigneeFirstName(String assigneeFirstName) {
        this.assigneeFirstName = assigneeFirstName;
    }

    public String getAssigneeLastName() {
        return assigneeLastName;
    }

    public void setAssigneeLastName(String assigneeLastName) {
        this.assigneeLastName = assigneeLastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public String getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public void setDesiredResolutionDate(String desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }



    public String getOwnerFirstName() {
        return ownerFirstName;
    }

    public void setOwnerFirstName(String ownerFirstName) {
        this.ownerFirstName = ownerFirstName;
    }

    public String getOwnerLastName() {
        return ownerLastName;
    }

    public void setOwnerLastName(String ownerLastName) {
        this.ownerLastName = ownerLastName;
    }

    public String getApproverFirstName() {
        return approverFirstName;
    }

    public void setApproverFirstName(String approverFirstName) {
        this.approverFirstName = approverFirstName;
    }

    public String getApproverLastName() {
        return approverLastName;
    }

    public void setApproverLastName(String approverLastName) {
        this.approverLastName = approverLastName;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getCategoryNum() {
        return categoryNum;
    }

    public void setCategoryNum(Long categoryNum) {
        this.categoryNum = categoryNum;
    }

    public List<AttachmentDTO> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentDTO> attachments) {
        this.attachments = attachments;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public ActionDTO getAvailableActions() {
        return availableActions;
    }

    public void setAvailableActions(ActionDTO availableActions) {
        this.availableActions = availableActions;
    }

    public boolean isFeedbackIsAvailable() {
        return feedbackIsAvailable;
    }

    public void setFeedbackIsAvailable(boolean feedbackIsAvailable) {
        this.feedbackIsAvailable = feedbackIsAvailable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketDTO ticketDTO = (TicketDTO) o;
        return feedbackIsAvailable == ticketDTO.feedbackIsAvailable &&
                Objects.equals(id, ticketDTO.id) &&
                Objects.equals(name, ticketDTO.name) &&
                Objects.equals(description, ticketDTO.description) &&
                Objects.equals(createdOn, ticketDTO.createdOn) &&
                Objects.equals(desiredResolutionDate, ticketDTO.desiredResolutionDate) &&
                state == ticketDTO.state &&
                urgency == ticketDTO.urgency &&
                Objects.equals(assigneeFirstName, ticketDTO.assigneeFirstName) &&
                Objects.equals(assigneeLastName, ticketDTO.assigneeLastName) &&
                Objects.equals(ownerId, ticketDTO.ownerId) &&
                Objects.equals(ownerFirstName, ticketDTO.ownerFirstName) &&
                Objects.equals(ownerLastName, ticketDTO.ownerLastName) &&
                Objects.equals(approverFirstName, ticketDTO.approverFirstName) &&
                Objects.equals(approverLastName, ticketDTO.approverLastName) &&
                Objects.equals(category, ticketDTO.category) &&
                Objects.equals(categoryNum, ticketDTO.categoryNum) &&
                Objects.equals(attachments, ticketDTO.attachments) &&
                Objects.equals(comments, ticketDTO.comments) &&
                Objects.equals(text, ticketDTO.text) &&
                Objects.equals(availableActions, ticketDTO.availableActions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, createdOn, desiredResolutionDate, state, urgency, assigneeFirstName, assigneeLastName, ownerId, ownerFirstName, ownerLastName, approverFirstName, approverLastName, category, categoryNum, attachments, comments, text, availableActions, feedbackIsAvailable);
    }
}
