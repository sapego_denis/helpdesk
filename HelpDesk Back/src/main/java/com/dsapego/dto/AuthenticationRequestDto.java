package com.dsapego.dto;


import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class AuthenticationRequestDto {
    @Email(message ="That's not email")
    @NotNull
    @Length(max = 100)
    private String username;
    @Length (min =6, max = 20)
    @NotNull
    @Pattern(regexp = "(?:[A-Z]{1,})(?:[~!@#$<>%^&*()_+-]{1,})(?:[a-z]{1,})(?:[0-9]{1,})")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
