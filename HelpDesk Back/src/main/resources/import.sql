INSERT INTO users(first_name, last_name, role, email, password) VALUES('Denis','DenisPetrLastName','EMPLOYEE','user1_mogilev@yopmail.com', '$2a$04$TmuUz7k98184BrR9KxGQ3uQF71BqKtQ7RJ/m6GjVtc4YkafRjIeZK');
INSERT INTO users(first_name, last_name, role, email, password) VALUES('Ivan','IvanPetrLastName','EMPLOYEE','user1_mogilev@yopmail.com', '$2a$04$TmuUz7k98184BrR9KxGQ3uQF71BqKtQ7RJ/m6GjVtc4YkafRjIeZK');
INSERT INTO users(first_name, last_name, role, email, password) VALUES('Petr','PetrLastName','MANAGER','manager1_mogilev@yopmail.com','$2a$04$TmuUz7k98184BrR9KxGQ3uQF71BqKtQ7RJ/m6GjVtc4YkafRjIeZK');
INSERT INTO users(first_name, last_name, role, email, password) VALUES('Simeon','SimeonLastName','MANAGER','manager2_mogilev@yopmail.com','$2a$04$TmuUz7k98184BrR9KxGQ3uQF71BqKtQ7RJ/m6GjVtc4YkafRjIeZK');
INSERT INTO users(first_name, last_name, role, email, password) VALUES('Aleks','AleksLastName','ENGINEER','engineer1_mogilev@yopmail.com','$2a$04$TmuUz7k98184BrR9KxGQ3uQF71BqKtQ7RJ/m6GjVtc4YkafRjIeZK');
INSERT INTO users(first_name, last_name, role, email, password) VALUES('Maks','MaksLastName','ENGINEER','engineer2_mogilev@yopmail.com','$2a$04$TmuUz7k98184BrR9KxGQ3uQF71BqKtQ7RJ/m6GjVtc4YkafRjIeZK');

INSERT INTO categories (name) VALUES('Application & Services');
INSERT INTO categories (name) VALUES('Benefits & Paper Work');
INSERT INTO categories (name) VALUES('Hardware & Software');
INSERT INTO categories (name) VALUES('People Management');
INSERT INTO categories (name) VALUES('Security & Access');
INSERT INTO categories (name) VALUES('Workplaces & Facilities');


INSERT INTO tickets (name, description, created_on, desired_resolution_date, state_id, urgency_id, assignee_id, owner_id, category_id) VALUES ('ticket1','Test description', CURRENT_DATE, CURRENT_DATE, 'NEW', 'CRITICAL',2, 1, 1);
INSERT INTO tickets (name, description, created_on, desired_resolution_date, state_id, urgency_id, assignee_id, owner_id, category_id) VALUES ('ticket2','Test description', CURRENT_DATE, CURRENT_DATE, 'DRAFT', 'LOW', 1, 2, 1);
INSERT INTO tickets (name, description, created_on, desired_resolution_date, state_id, urgency_id, assignee_id, owner_id, category_id) VALUES ('ticket2','Test description', CURRENT_DATE, CURRENT_DATE, 'DRAFT', 'LOW', 1, 1, 1);

INSERT INTO comments (text, dateOfComment, user_id, ticket_id) VALUES('Test comments', CURRENT_DATE, 1, 1);

INSERT INTO histories (ticketAction, dateOfStory, user_id, ticket_id) VALUES('Test history', CURRENT_DATE, 1, 1);