import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.Category;

import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.ticket.Urgency;
import com.dsapego.model.user.Role;
import com.dsapego.model.user.User;
import com.dsapego.repository.TicketRepository;

import com.dsapego.services.UserDetailService;
import com.dsapego.services.UserService;
import com.dsapego.services.impl.TicketServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verifyNoMoreInteractions;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TicketServiceImplTest {

    @Mock
    private UserService userService;

    @Mock
    private UserDetailService userDetailService;

    @Mock
    private TicketRepository ticketRepository;


    @InjectMocks
    private TicketServiceImpl ticketService;

    private User user;
    private Ticket ticket;
    private Category category;


    @Before
    public void init() {
        user = new User();
        user.setLastName("Last name test");
        user.setFirstName("First name test");
        user.setRole(Role.MANAGER);
        user.setEmail("test@test.com");

        category = new Category();
        category.setId(Long.MAX_VALUE);
        category.setName("TestCategory");

        ticket = new Ticket();
        ticket.setId(1L);
        ticket.setCreatedOn(LocalDateTime.now());
        ticket.setName("testTicketName");
        ticket.setOwnerID(user);
        ticket.setUrgencyId(Urgency.AVERAGE);
        ticket.setAssigneeID(user);
        ticket.setApproverID(user);
        ticket.setCategoryID(category);

        Mockito.when(userDetailService.getCurrentUser()).thenReturn(user);
        Mockito.when(ticketRepository.getByKey(1L)).thenReturn(Optional.ofNullable(ticket));

    }
    @Test
    public void findAllTicketsForManagerRole() {
        Mockito.when(userDetailService.getCurrentUser()).thenReturn(user);
        List tickets = new ArrayList();
        tickets.add(ticket);
        Mockito.when(ticketRepository.findAllTicketsForManager(user)).thenReturn(tickets);
        assertThat(ticketService.findTicketsAccordingToRole())
                .isNotNull()
                .isInstanceOf(ArrayList.class)
                .isNotEmpty()
                .hasSize(1)
                .extracting("ownerID", "id", "name", "categoryID")
                .contains(tuple(user, 1L, "testTicketName", category));

        verify(userDetailService, atLeastOnce()).getCurrentUser();
        verify(ticketRepository, times(1)).findAllTicketsForManager(user);
        verifyNoMoreInteractions(userService);
        verifyNoMoreInteractions(ticketRepository);

    }

    @Test
    public void findTicketByIdWithCorrectIdTest() throws NotFoundException {

        Mockito.when(ticketRepository.getByKey(1L)).thenReturn(Optional.ofNullable(ticket));

        assertThat(ticketService.findTicketByID(1L))
        .isNotNull()
        .isInstanceOf(Ticket.class)
        .extracting(Ticket::getId)
        .isEqualTo(1L);
    }



    @Test (expected = NotFoundException.class)
    public void fsd() throws NotFoundException {
        when(ticketRepository.getByKey(anyLong())).thenReturn(null);
        ticketService.findTicketByID(anyLong());
    }

}