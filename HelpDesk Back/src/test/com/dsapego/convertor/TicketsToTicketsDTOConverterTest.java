package com.dsapego.convertor;


import com.dsapego.dto.ActionDTO;
import com.dsapego.dto.AttachmentDTO;
import com.dsapego.dto.TicketDTO;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.Attachment;
import com.dsapego.model.Category;
import com.dsapego.model.ticket.State;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.ticket.Urgency;
import com.dsapego.model.user.User;
import com.dsapego.repository.CategoryRepository;
import com.dsapego.repository.UserRepository;
import com.dsapego.services.ActionDTOService;
import com.dsapego.services.UserDetailService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class TicketsToTicketsDTOConverterTest {

@Mock
private Category category;

    @Mock
    private User user;

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private List<Attachment> attachments;

    @Mock
    private List<AttachmentDTO> attachmetsDTO;

    @Mock
    private AttachmentToAttachmentDTOConverter attachmentConverter;

    @Mock
    private UserDetailService userDetailService;

    @Mock
    private ActionDTOService actionDTOService;
    Set<String> availableActions;

    @InjectMocks
    private TicketsToTicketsDTOConverter converter;

    @Before
    public void setUp() throws NotFoundException {

        MockitoAnnotations.initMocks(this);

        Mockito.when(categoryRepository.getByKey(1L)).thenReturn(Optional.of(category));
        Mockito.when(userRepository.getByKey(1L)).thenReturn(Optional.of(user));
        Mockito.when(userDetailService.getCurrentUser()).thenReturn(user);
        Mockito.when(attachmentConverter.convert(attachments)).thenReturn(attachmetsDTO);
        Mockito.when(actionDTOService.getAvailableActions(1L)).thenReturn((ActionDTO) availableActions);

    }

    @Test
    public void convert() {
        LocalDateTime createdOn = LocalDateTime.now();
        String resolutionDate ="2020-06-24T21:00:00.000Z";

        TicketDTO expectedTicketDTO = new TicketDTO();
        expectedTicketDTO.setCreatedOn(createdOn);
        expectedTicketDTO.setId(1L);
        expectedTicketDTO.setName("Ticket testName ");
        expectedTicketDTO.setDescription("Test description");
        expectedTicketDTO.setCreatedOn(createdOn);
        expectedTicketDTO.setDesiredResolutionDate(resolutionDate);
        expectedTicketDTO.setState(State.APPROVED);
        expectedTicketDTO.setUrgency(Urgency.AVERAGE);
    //    expectedTicketDTO.setOwnerId("1");
     //   expectedTicketDTO.setCategoryNum(1L);
       // expectedTicketDTO.setAvailableActions((ActionDTO) availableActions);
        expectedTicketDTO.setAttachments(attachmetsDTO);
        expectedTicketDTO.setFeedbackIsAvailable(false);

        Ticket ticket = new Ticket();
        DateTimeFormatter dTF = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        ticket.setId(1L);
        ticket.setName("Ticket testName ");
        ticket.setDescription("Test description");
        ticket.setCreatedOn(createdOn);
        ticket.setDesiredResolutionDate(LocalDateTime.parse(resolutionDate,dTF));
        ticket.setStateId(State.APPROVED);
        ticket.setUrgencyId(Urgency.AVERAGE);
        ticket.setOwnerID(user);
        ticket.setCategoryID(category);
        ticket.setAttachments(attachments);


        TicketDTO actualTicketDTO = converter.convert(ticket);

        Assert.assertEquals(expectedTicketDTO, actualTicketDTO);




    }
}
