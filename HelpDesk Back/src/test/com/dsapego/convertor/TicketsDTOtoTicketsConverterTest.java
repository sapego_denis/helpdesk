package com.dsapego.convertor;

import com.dsapego.dto.TicketDTO;
import com.dsapego.exceptions.NotFoundException;
import com.dsapego.model.Category;

import com.dsapego.model.ticket.State;
import com.dsapego.model.ticket.Ticket;
import com.dsapego.model.ticket.Urgency;
import com.dsapego.model.user.User;
import com.dsapego.repository.CategoryRepository;
import com.dsapego.repository.UserRepository;
import com.dsapego.services.CategoryService;
import com.dsapego.services.UserDetailService;
import com.dsapego.services.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;



@RunWith(MockitoJUnitRunner.class)
public class TicketsDTOtoTicketsConverterTest {


    @Mock

    private Category category;

    @Mock
    private User user;

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private CategoryService categoryService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserService userService;

    @Mock
    private UserDetailService userDetailService;

    @InjectMocks
    private TicketsDTOtoTicketsConverter converter;

    @Before
    public void setUp() throws NotFoundException {

        MockitoAnnotations.initMocks(this);
        Mockito.when(categoryService.findCategoryByID(1L)).thenReturn(category);
        Mockito.when(userService.findUserById(1L)).thenReturn(user);

    }

    @Test
    public void convert() {
        LocalDateTime createdOn = LocalDateTime.now();
        String resolutionDate ="2020-06-24T21:00:00.000Z";

        TicketDTO ticketDTO = new TicketDTO();
        ticketDTO.setCreatedOn(createdOn);
        ticketDTO.setId(1L);
        ticketDTO.setName("Ticket testName ");
        ticketDTO.setDescription("Test description");
        ticketDTO.setCreatedOn(createdOn);
        ticketDTO.setDesiredResolutionDate(resolutionDate);
        ticketDTO.setState(State.APPROVED);
        ticketDTO.setUrgency(Urgency.AVERAGE);
        ticketDTO.setOwnerId("1");
        ticketDTO.setCategoryNum(1L);
        ticketDTO.setCategory(category);


        Ticket expectedTicket = new Ticket();
        DateTimeFormatter dTF = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
        expectedTicket.setId(1L);
        expectedTicket.setName("Ticket testName ");
        expectedTicket.setDescription("Test description");
        expectedTicket.setCreatedOn(createdOn);
        expectedTicket.setDesiredResolutionDate(LocalDateTime.parse(resolutionDate,dTF));
        expectedTicket.setStateId(State.APPROVED);
        expectedTicket.setUrgencyId(Urgency.AVERAGE);
        expectedTicket.setOwnerID(user);
        expectedTicket.setCategoryID(category);


        Ticket actualTicket = converter.convert(ticketDTO);

        Assert.assertEquals(expectedTicket, actualTicket);




    }
}