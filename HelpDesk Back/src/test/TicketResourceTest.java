import com.dsapego.dto.TicketDTO;
import com.dsapego.config.AppConfig;
import com.dsapego.model.ticket.State;
import com.dsapego.model.ticket.Urgency;
import com.dsapego.web.TicketResource;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.time.LocalDateTime;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@WebAppConfiguration
public class TicketResourceTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private TicketResource controller;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    @Test
    public void contexLoads()  {
        assertThat(controller).isNotNull();
    }
    @Test
   public void addTicketTest() throws Exception {
      LocalDateTime createdOn = LocalDateTime.now();
      String resolutionDate ="2020-06-24T21:00:00.000Z";
      final TicketDTO ticketDTO = new TicketDTO();

        ticketDTO.setName("Ticket testName ");
        ticketDTO.setDescription("Test description");
        ticketDTO.setCreatedOn(createdOn);
        ticketDTO.setDesiredResolutionDate(resolutionDate);
        ticketDTO.setState(State.APPROVED);
        ticketDTO.setUrgency(Urgency.AVERAGE);
        ticketDTO.setOwnerId("1");
        ticketDTO.setCategoryNum(1L);

       final ObjectMapper objectMapper = new ObjectMapper();
        mockMvc.perform(post("/v1/tickets/")
                .contentType("application/json;charset=UTF-8")
                .content(objectMapper.writeValueAsString(ticketDTO)))
                .andDo(print())
                .andExpect(status().isOk());

   }




}
